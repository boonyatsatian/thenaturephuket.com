<?php include("../application/views/front/meta.php");?>
<?php include("../application/views/front/header.php");?>



<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center">
                    <h1>Location</h1>
                    <h2>The Aquamarine Resort & Villa Promotion Special Offer</h2>
                    <p>Aquamarine Resort & Villas is in a tranquil hillside location overlooking Kamala beach on the south west coast of Phuket, about one hour's drive from Phuket International Airport, giving our guests beautiful sea views.
                        Shopping and all sorts of fun in the sun plus the island's most vibrant night life is just a short drive away at the nearby Patong Beach.</p>
                </div>
                <div class="row">
                    <iframe width="100%" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.th/maps/ms?msa=0&amp;msid=210541351709886234537.0004dc1bd6771e4c139eb&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=7.946017,98.268242&amp;spn=0.038253,0.074415&amp;z=14&amp;output=embed"></iframe><br /><small>View <a href="https://maps.google.co.th/maps/ms?msa=0&amp;msid=210541351709886234537.0004dc1bd6771e4c139eb&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=7.946017,98.268242&amp;spn=0.038253,0.074415&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left">Aquamarine Resort &amp; Villa</a> in a larger map</small>
                </div>

            </div>
        </div>
    </div>
</section>


<?php include("../application/views/front/footer.php");?>

