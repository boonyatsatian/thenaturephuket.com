//=============================//
//======Create Age Option======//
var ageOption = '';

for(var age = 1; age < 13; age++)
{
    ageOption += '<option value="'+age+'">'+age+'</option>';
}

//======Create Total Adult Option======//
var totalAdultOption = '';
for(var totalAdult = 1; totalAdult <= 6; totalAdult++)
{
    totalAdultOption += '<option value="'+totalAdult+'">'+totalAdult+'</option>';
}

//======Create Total Child Option======//
var totalChildOption = '';
for(var totalChild = 0; totalChild <= 6; totalChild++)
{
    totalChildOption += '<option value="'+totalChild+'">'+totalChild+'</option>';
}

//=============================//
var roomsOption = '';

for(var room = 1; room < 5; room++)
{
    roomsOption += '<option value="'+room+'">'+room+'</option>';
}
//=============================//

$(document).ready(function() {
    $(".lightbox").lightbox();

    $('#btn-booknow').click(function(){
        var indate = $('#indate').val();
        var outdate = $('#outdate').val();

        $('#indateModal').html(indate);
        $('#outdateModal').html(outdate);

        $('#modalBookNow').modal('show');
    });

    var currentTime = new Date();
    var smonth = currentTime.getMonth() + 1;
    var smonth = (smonth < 10) ? '0' + smonth : smonth;
    var sday = currentTime.getDate();
    var sday = (sday < 10) ? '0' + sday : sday;
    var syear = currentTime.getFullYear();
    $("#indate").val(syear + "-" + smonth + "-" +sday );

    var outdate = new Date(currentTime.setDate(currentTime.getDate() + 1));
    var emonth = outdate.getMonth() + 1;
    var emonth = (emonth < 10) ? '0' + emonth : emonth;
    var eday = currentTime.getDate();
    var eday = (eday < 10) ? '0' + eday : eday;
    var eyear = currentTime.getFullYear();

    $("#outdate").val(eyear + "-" + emonth + "-" +eday);
    $("#indate").datepicker({
        minDate: 0,
        onSelect: customStart,
        dateFormat: "yy-mm-dd",
        firstDay: 1,
        changeFirstDay: false
    });
    $("#outdate").datepicker({
        beforeShow: customEnd,
        dateFormat: "yy-mm-dd",
        firstDay: 1,
        changeFirstDay: false
    });


});

function parseDate(input)
{
    var parts = input.match(/(\d+)/g);
    // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
    return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
}

function customStart(input)
{
    var newDate = $(this).datepicker("getDate");
    if (newDate) {
     newDate.setDate(newDate.getDate() + 1);
    }
    $("#outdate").datepicker().datepicker("enable");
    $("#outdate").datepicker("setDate", newDate);
    dateDifference();
}


function customEnd(input)
{
    var newDate = $("#indate").datepicker("getDate");

    if (newDate)
    {
        newDate.setDate(newDate.getDate() + 1);
    }

    $("#outdate").datepicker("option", "minDate", newDate);
}


function available(frmObj)
{
    //======================================================//
    //==================Check No. of Adult==================//
    var noOfRoom = $('#chknoofroom').val();
    var noOfAdult = $('#chknoadult').val();

    if(noOfRoom > noOfAdult)
    {
        alert("Number fo Adult must be " + noOfRoom + " or more.");
        $('#chknoadult').focus();
        return false;
    }
    //======================================================//

    var form = document.getElementById(frmObj);
    form.setAttribute("method", "get");
    form.setAttribute("action", "https://ibe.hoteliers.guru/ibe/search-room-rate");

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "hotel");
    HotelhiddenField.setAttribute("name", "hotel");
    HotelhiddenField.setAttribute("value", "2");
    form.appendChild(HotelhiddenField);

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "sd");
    HotelhiddenField.setAttribute("name", "sd");
    HotelhiddenField.setAttribute("value", $('#indate').val());
    form.appendChild(HotelhiddenField);

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "ed");
    HotelhiddenField.setAttribute("name", "ed");
    HotelhiddenField.setAttribute("value", $('#outdate').val());
    form.appendChild(HotelhiddenField);

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "rooms");
    HotelhiddenField.setAttribute("name", "rooms");
    HotelhiddenField.setAttribute("value",  $('#chknoofroom :selected').val());
    form.appendChild(HotelhiddenField);

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "adult");
    HotelhiddenField.setAttribute("name", "adult");
    HotelhiddenField.setAttribute("value", $('#chknoadult :selected').val());
    form.appendChild(HotelhiddenField);

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "child");
    HotelhiddenField.setAttribute("name", "child");
    HotelhiddenField.setAttribute("value",  $('#chknochild :selected').val());
    form.appendChild(HotelhiddenField);

    //======================================================//
    //===============Generate Age Of Children===============//
    var ageOfChildrenValue = '';
    var currentNoOfChildren = $('#current_no_children').val();
    currentNoOfChildren = parseInt(currentNoOfChildren);

    for(var num = 1; num <= currentNoOfChildren; num++)
    {
        ageOfChildrenValue += $('#age_of_children'+num).val();

        if(num < currentNoOfChildren)
        {
            ageOfChildrenValue += '|';
        }
    }

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "childage");
    HotelhiddenField.setAttribute("name", "childage");
    HotelhiddenField.setAttribute("value",  ageOfChildrenValue);
    form.appendChild(HotelhiddenField);
    //======================================================//

    form.submit();
}

function dateDifference()
{
    var strDate1=document.getElementById("indate").value;
    var strDate2=document.getElementById("outdate").value;
    datDate1= parseDate(strDate1);
    datDate2= parseDate(strDate2);

    if(((datDate2-datDate1)/(24*60*60*1000))>=0)
    {
        document.getElementById("night").value=((datDate2-datDate1)/(24*60*60*1000));

    }
}

function displayAgeOfChildren2(noOfChildren, numOfRoom) {
    $('#clearfixAgeOfChildren').addClass('clearfix');

    var currentNoOfChildren = $('#current_no_children'+numOfRoom).val();

    $('#current_no_children'+numOfRoom).val(noOfChildren);

    if(noOfChildren == 0)
    {
        $('#ageRow'+numOfRoom).fadeOut();
    }

    if(parseInt(noOfChildren) < parseInt(currentNoOfChildren)) {
        for(var i = parseInt(noOfChildren); i <= parseInt(currentNoOfChildren); i++) {
            $('#field_age_of_child' + numOfRoom + i).remove();
        }
    }
    else {
        var ageOfChildrenTag = '';
        for(var num = parseInt(currentNoOfChildren); num < parseInt(noOfChildren); num++) {
            ageOfChildrenTag += //'<div id="field_age_of_child' + numOfRoom + num + '"> <span class="text1">Child ' + parseInt(num+1) + '</span>'
            //    +  '<select id="age_of_child' + parseInt(numOfRoom-1) + num + '" name="age_of_child[' + parseInt(numOfRoom-1) +']['+ num+ ']">'
            //    +  ageOption
            //    +  '</select>'
            //    +  '</div>';
            '<div class="col-sm-4" id="field_age_of_child' + numOfRoom + num + '">'+
            '<div class="form-group">'+
            '<div class="input-group select-no-rd select-wrapper">'+
            ' <div class="input-group-addon">Child ' + parseInt(num+1) + '</div>'+
            '<select id="age_of_child' + parseInt(numOfRoom-1) + num + '" name="age_of_child[' + parseInt(numOfRoom-1) +']['+ num+ ']" class="form-control no-radius">'+
            ageOption +
            '</select>'+
            '</div>'+
            '</div>'+
            '</div>';
        }

        $('#ageOfChildren'+numOfRoom).append(ageOfChildrenTag);

        if($('#ageRow'+numOfRoom).css('display') == 'none')
        {
            $('#ageRow'+numOfRoom).fadeIn();
        }
    }
}

//=====================================================//
//===============Display Age Of Children===============//
function displayAgeOfChildren(noOfChildren, numOfRoom)
{
    var currentNoOfChildren = $('#current_no_children').val();
    var currentnoOfRoom = $('#current_no_rooms').val();
    noOfChildren = parseInt(noOfChildren);
    currentNoOfChildren = parseInt(currentNoOfChildren);


    if(noOfChildren == 0)
    {
        if($('#ageRow'+numOfRoom).css('display') != 'none')
        {
            $('#current_no_children').val(0);
            $('#ageOfChildren'+numOfRoom).html('');
            $('#ageRow'+numOfRoom).fadeOut();
        }
    }
    else
    {
        if(noOfChildren > currentNoOfChildren)
        {
            currentNoOfChildren++;
            var ageOfChildrenTag = '';

            for(var num = currentNoOfChildren; num <= noOfChildren; num++)
            {
                ageOfChildrenTag += '<div id="field_age_of_children' + numOfRoom + noOfChildren + '"> ' + num + '.'
                                 +  '<select id="age_of_children' + num + '" name="age_of_children' + num + '"class="form-control no-radius">'
                                 +  ageOption
                                 +  '</select>'
                                 +  '</div>';
            }

            $('#ageOfChildren'+numOfRoom).append(ageOfChildrenTag);
        }
        else
        {
            for(var num = noOfChildren+1; num <= currentNoOfChildren; num++)
            {
                $('#field_age_of_children' + numOfRoom + noOfChildren).remove();
            }
        }

        $('#current_no_children').val(noOfChildren);

        if($('#ageRow'+numOfRoom).css('display') == 'none')
        {
            $('#ageRow'+numOfRoom).fadeIn();
        }
    }
}
//=====================================================//

//===============Display Number Of Rooms ===============//
function displayNumberOfrooms(noOfRoom, page)
{
    if(page == 'main-page') {
        var selectValue = $('#chknoofroom').find(":selected").val();
        $('#chknoofroomModal option[value="'+selectValue+'"]').attr('selected', 'selected');
    }
    else {
        var selectValue = $('#chknoofroomModal').find(":selected").val();
        $('#chknoofroom option[value="'+selectValue+'"]').attr('selected', 'selected');
    }

    if(noOfRoom == 1) {
        $('#numberrroomRow').css({"display": "none"});
        $("#adultRoom1").css({
            "float" : "left",
            "margin" :"5px"}
        );
        $("#childRoom1").css({
            "float" : "left",
            "margin" :"5px"}
        );
    }
    else {
        $('#numberrroomRow').css({"display": "block"});
        $("#adultRoom1").removeAttr("style");
        $("#childRoom1").removeAttr("style");
    }

    var currentnoOfRoom = $('#current_no_rooms').val();
    noOfRoom = parseInt(noOfRoom);
    currentnoOfRoom = parseInt(currentnoOfRoom);

    if(noOfRoom == 0)
    {
        if($('#numberrroomRow').css('display') != 'none')
        {
            $('#current_no_rooms').val(1);
            $('#numOfroom').html('');
            $('#numberrroomRow').fadeOut();
        }
    }
    else
    {
        if(noOfRoom > currentnoOfRoom)
        {
            currentnoOfRoom++;
            var numOfroomTag = '';

            for(var num = currentnoOfRoom; num <= noOfRoom; num++)
            {
                numOfroomTag += '<div id="field_total_adult_children'+num+'">' +
                '<div class="clearfix"></div>' +
                '<div style="width: 100%;">'+
                '<div id="numberrroomRow" class="text1">Number Of Room <span id="numberOfRoom'+num+'">'+num+'/'+noOfRoom+'</span></div>'+
                '<div class="col-sm-6">'+
                    '<div class="form-group">'+
                        '<div id="adultRoom1" class="input-group select-no-rd select-wrapper">'+
                            '<div class="input-group-addon">Adults</div>'+
                                '<select id="chknoadult" class="form-control no-radius" name="total_adult['+parseInt(num-1)+']">'+
                                totalAdultOption+
                                '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-sm-6">'+
                    '<div class="form-group">'+
                        '<div id="childRoom1" class="input-group select-no-rd select-wrapper">'+
                            '<div class="input-group-addon">Children</div>'+
                             '<select id="chknochild" class="form-control no-radius" name="total_child['+parseInt(num-1)+']" onchange="javascript: displayAgeOfChildren2(this.value,'+num+');">'+
                                 totalChildOption+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                 '</div>'+


                '<span id="ageRow'+num+'" style="display: none; float: left;" class="text2">Age of Children (Yrs)</span>' +
                    '<div id="ageOfChildren'+num+'" style="float: left;"></div>' +
                        '<input type="hidden" id="current_no_children'+num+'" value="0">' +
                '</div>'+
                '</div>';
            }

            $('#total_adult_child').append(numOfroomTag);
        }
        else
        {
            for(var num = noOfRoom+1; num <= currentnoOfRoom; num++)
            {
                $('#field_total_adult_children'+num).remove();
            }
        }

        $('#current_no_rooms').val(noOfRoom);
    }

    for(var i = 1; i <= noOfRoom; i++){
        $('#numberOfRoom'+i).html(i+'/'+noOfRoom);
    }
}
//=====================================================//