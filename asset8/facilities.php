<?php include("../application/views/front/meta.php");?>
<?php include("../application/views/front/header.php");?>



<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center">
                    <h1>Facilities</h1>
                    <h2>Our resort comprises 186 guest rooms and 16 private villas.</h2>
                </div>
                <p>Other on-site facilities include three swimming pools, a fitness room, a mini-mart and gift shop, a tailor, tour centre and internet corner. A free daily shuttle services operates to Kamala Beach and Patong Beach.<br/><br/>

                    <strong>See our full list of services below</strong>

                    - 3 outdoor pools (lap pool, Jacuzzi pools and free-form pool)<br/>
                    - Children's pool<br/>
                    - 2 Restaurants, lobby lounge and pool bars<br/>
                    - Romantic Package "Romantic dinner in private locations<br/>
                    - Wi-Fi available at the main lobby (chargeable)<br/>
                    - In room hi-speed internet (free of charge)<br/>
                    - Free shuttle to Kamala beach at 11:00 hrs.,13:00 hrs., 15:00 hrs. and 17:00 hrs.<br/>
                    - Free shuttle to Patong beach at 11:30 hrs.,17:30 hrs. and 21:30 hrs. and from Patong beach to hotel at 18:00 hrs. and 22:00 hrs.<br/>
                    - Thai Cooking classes<br/>
                    - Fitness Centre<br/>
                    - Games room<br/>
                    - Tour information<br/>
                    - Gift Shop<br/>
                    - Tailor Shop<br/>
                    - Mini-mart<br/>
                    - 24 hours Duty Manager<br/>
                    - 24 hours security<br/>
                    - Guests Relations Officer<br/>
                    - Baby sitting service<br/>
                    - Laundry and dry cleaning service<br/>
                    - Private and self-drive car park<br/>
                    - Parking<br/>
                    - Medical service on call</p>
                <div class="row">
                    <div class="col-lg-3 space-for-promotion"><img src="images/facilities/1.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/facilities/2.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/facilities/3.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/facilities/4.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/facilities/5.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/facilities/6.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/facilities/7.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/facilities/8.jpg" width="100%"></div>
                </div>

            </div>
        </div>
    </div>
</section>


<?php include("../application/views/front/footer.php");?>

