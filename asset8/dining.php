<?php include("../application/views/front/meta.php");?>
<?php include("../application/views/front/header.php");?>



<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center">
                    <h1>Dining</h1>
                    <h2 class="space-between-h2-and-p">Blue Sea Cafe</h2>
                </div>
                <p>Dining at Aquamarine Resort is always a treat with continental buffet breakfast and dinner served at the open-air Blue Sea Cafe with a pool and sea view over Kamala Bay.
                    Dine by candlelight with our dinner buffet of authentic regional Thai cuisine and a variety of international favourites such as sushi, roasts and salads, plus a pasta chef who will whip up a dish of your choosing.<br/>
                </p>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-8 col-md-offset-2 "><hr/><div class="clearfix"></div></div>
        </div>


        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Leelawadee Thai Restaurant</h2>
                <p>Dine on authentic Thai cuisine and freshly-caught seafood cooked up by some of the best-known chefs in Thailand in air conditioned comfort with a very appetizing sunset sea view.<br/>
                    <strong>Open for Dinner 6.00pm – midnight. </strong>
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Horizon Lounge</h2>
                <p>One of the most stunning lobby lounges on the coast, it's worth taking your time to acquaint yourself with the palace-like grandeur of the Horizon Lounge and its spectacular sea view while you can relax with refreshing chilled herbal drinks, light meals and cocktails.<br/>
                    <strong>Refreshments served 10.00am – midnight. </strong>
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Swim-up Bar</h2>
                <p>Swim up to the pool bar for alcoholic and non-alcoholic refreshments while you chill from your perch in the pool overlooking the bay.
                    Room Service is also available with a menu of Thai and international dishes.<br/>
                    <strong>Open 10.00am – 6.00pm.</strong>
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>

    </div>
</section>


<?php include("../application/views/front/footer.php");?>\\\




