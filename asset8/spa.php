<?php include("../application/views/front/meta.php");?>
<?php include("../application/views/front/header.php");?>



<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center">
                    <h1>Lotus Spa</h1>
                    <h2>An Authentic Thai Spa Experience</h2>
                </div>
                <p>Treat yourself and you partner to our traditional Thai beauty and massage treatments with therapists skilled in the ancient arts of reflexology (foot massage), Thai massage of the joints and the use of Thai medicinal herbs for the boy, soul and skin.<br/><br/>
                    Each of our three secluded Lotus Spa Suites has a private changing room, herbal steam room, shower and spa tub among tranquil gardens, where you can gather your thoughts after a wondrous massage<br/><br/>
                    Our treatments offered by the spa include gentle exfoliation using blended scented face powder and petals from lotus combined with essential oils, is also ingeniously applied to provide a renewed and radiant glow to the skin.
                </p>
                <hr/>
            </div>
        </div>

        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Body Massage</h2>
                <p>
                    Therapy that uses manipulative and soft tissue techniques that are generally based on concept of the anatomy, physiology and human function, relaxes, creates a sense of well-being, eases strain and tension, mobilizes stiff joints, improves blood circulation, improve the digestive system, and encourages the removal of toxins from the body, generally delivered by hand, though machines and high-powered jets are also used.<br/><br/>
                    A technique that involves a unique combination of pressure point and stretching using a range of motion and acupressure technique without the use of oils.<br/><br/>
                    A back massage to loosen tight muscles in the neck, shoulders and backs.<br/><br/>
                    A technique that maintains that the body is divided into body zones, all of which have a corresponding reflex area on the foot, applying pressure to a particular massage point on the foot helps circulation, promotes relaxation and relieves pain in one of the body zones.<br/><br/>
                    Massage in which essential oils-either pre-blended or specially mixed are applied to the body, typically with Swedish massage techniques<br/><br/>
                    Deep massage technique to free congestion of the muscle attachments, Muscle function and awareness are balanced, and elasticity is revived.
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Body Treatments</h2>
                <p>
                    General term that denotes treatments for the body
                    Restore skin's radiant glow with an exfoliating sea salt enriched with Lavender. As these delightful aromas entice your senses, the body is gently polished, rendering skin silky-smooth and mineralized. Exfoliation is recommended as an ideal pre-treatment service in order to maximize body treatment results.<br/><br/>
                    Rejuvenating gentle body scrubs help to exfoliates dead skin cells and improves circulation, rendering your skin clean, smooth, revitalized and silky-soft by using traditional Thai herbal remedies.<br/><br/>
                    Treatment that soothes skin that has been over-exposed to the sun, and cools the over-heated body, may include a cooling compress and a gentle massage which may include soothing ingredients such as Cucumber and Aloe Vera.<br/><br/>
                    Formulated with revitalizing liquid algae, this treatment is rich in repairing natural vitamins and minerals that serve to eliminate accumulated deposits and restore elasticity. Your skin is rendered palpably resurfaced, tighter and firmer<br/><br/>
                    This treatment is rich in natural vitamins, minerals and enzymes, is generously applied to your entire body. Sore muscles, aches and pains are alleviated as your body's circulation is stimulated and stress vanishes, Ideal for those prone to chronic pain or fatigue, rheumatism / arthritis pain and post-sports injuries.
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">The Lotus Facial Treatment</h2>
                <p>
                    Treatment that cleanses and improves the complexion of the face using products that best suit a particular skin type, may include gentle exfoliation, steaming to open pores for extraction, application of a facial mask and moisturizer, and a facial massage. Types of facials include aromatic, oxygenating, whitening and deep cleansing facials.<br/><br/>
                    The ultimate in deep cleansing facials includes cleansing, exfoliation, facial massage, and a combination of two facial masks. Especially designed to refresh and nourish your complexion. Recommend for all skin types.<br/><br/>
                    This purely natural aromatic face treatment oil soothes and calms the skin. It also helps regulate moisture balance.<br/><br/>
                    You don't have to age gracefully or even apparently! Counteract the aging process with this proven-effective advance treatment specifically formulated for men. Rejuvenating and repairing collagen replenish skin while counteracting aging aggressors. Visibly resurfacing, this mask promotes healthy, youthful skin.<br/><br/>
                    Formulated for dry and dehydrated skins, this dual action treatment nourishes and hydrates the driest of skin. Using the purest active ingredients and essential oils from plants, this facial smoothes fine lines and diminishes wrinkles. Its long-term effectiveness reinforces the epidermis to be better equipped to combat aggressive climactic conditions.
                    This powerful treatment restores skin's activity and increases the absorption of concentrated ampoule specifically chosen to suit your skin type.
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>


        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Spa Package</h2>
                <p>
                    Treatment combinations : Certain body treatments, when taken in combination, can provide greater results than taken separately or individually. So we have specially designed these Spa Packages for your best benefits and optimum spa experiences.<br/>
                    - Thai Herbal Steam<br/>
                    - Aromatherapy Salt Glow or Ancient Thai Cooling Scrub<br/>
                    - Tension Relief Massage - Thai Herbal Steam<br/>
                    - Thai Herbal Jacuzzi<br/>
                    - Ancient Thai Cooling Scrub<br/>
                    - Traditional Thai Massage - Thai Herbal Steam<br/>
                    - Thai Herbal Jacuzzi<br/>
                    - Deep Tissue "Sport" Massage<br/>
                    - Skin Fit for Him - Romancing Thai Herbal Steam<br/>
                    - Romancing Thai Herbal Jacuzzi<br/>
                    - Aromatherapy Salt Glow or Ancient Thai Cooling Scrub<br/>
                    - Aromatherapy Massage or Deep Tissue "Sport" Massage - Thai Herbal Steam<br/>
                    - Thai Herbal Jacuzzi<br/>
                    - Aromatherapy Salt Glow or Ancient Thai Cooling Scrub<br/>
                    - Aromatic Seaweed Body Wrap or Firming Treatment<br/>
                    - Aromatherapy Massage or Deep Tissue "Sport" Massage<br/>
                    - Aromatherapy Facial or Skin Fit for Him
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>


        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Nail Treatment</h2>
                <p>
                    Treatment provided by spas to enhance beauty and overall well-being, include manicure and pedicure treatments.
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>


        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Steam & Jacuzzi Bath</h2>
                <p>
                    Relive stress in steam and Jacuzzi bath using Thai Herbal to relieving stress properties
                    <strong>Cancellation Policy :-</strong>
                    Subject to space available, all rescheduling of appointments must be done 6 hours in advance. Cancellations made less than 2 hours will incur a 50% cancellation charge based on the treatment reserved. All no shows will be charged with the full amount.
                    <strong>Operation Hours : </strong>09:00 a.m. - 07:00 p.m.
                    For more information, please contact our spa receptionist. Ext.8209
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>


        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Frequently Asked Questions</h2>
                <p>
                    <strong>WHAT ARE THE OPENING HOURS OF THE SPA?</strong><br/>
                    - The Spa is open from 09.00 A.M. to 07.00 P.M.<br/><br/>
                    <strong>WHAT SHOULD I WEAR TO THE SPA?</strong><br/>
                    - Wear whatever you feel comfortable in. We will provide you with a bathrobe and pyjamas.<br/><br/>
                    <strong>SHOULD I MAKE A RESERVATION IN ADVANCE?</strong><br/>
                    - Advance booking prior to your arrival is recommended to secure your preferred date and time of
                    treatment.<br/><br/>
                    <strong>WHEN SHOULD I ARRIVE?</strong>
                    - Please check in at the spa reception at least 15 minutes prior to your scheduled appointment to
                    avoid reduced treatment times.<br/><br/>
                    <strong>WHO CAN HELP ME DECIDE HOW TO CHOOSE MY TREATMENT?</strong><br/>
                    - Our Spa Manager or Spa Receptionist will help you plan the perfect spa experience, including the
                    best order for your treatment. Spa package are specially designed to offer you a selection of our
                    most popular spa treatment.<br/><br/>
                    <strong>WHERE DO I CHANGE CLOTHES?</strong><br/>
                    - You may either change in the spa private suite.<br/><br/>
                    <strong> WHAT IF A HAVE SPECIAL HEALTH CONSIDERATION?</strong><br/>
                    - Guests who have high blood pressure, heart conditions, allergies, are pregnant or have any other
                    medical complications are devised to consult their doctors before signing up for any spa service.
                    Please notify our Spa Manager or Spa Receptionist aware of any medical conditions.<br/><br/>
                    <strong>WHAT DO I WEAR DURING MY TREATMENT?</strong><br/>
                    - Most body treatments are enjoyed without clothing. However, wear what you feel is most comfortable
                    for you. We provide special pyjamas for Thai Massage.<br/><br/>
                    <strong>WHAT ABOUT MY COMFORT?</strong><br/>
                    - This is your time in the spa and you should delight in it as much as possible. Whether the room is
                    too hot, the music is too loud, the lights too bright or the pressure of the massage
                    uncomfortable…just let us know.<br/><br/>
                    - To ensure that guests can enjoy the peaceful sanctuary of The Lotus Spa, we respectfully request
                    that all visitors keep noise to a minimum. Cellular phones and electronic devices are discouraged.<br/><br/>
                    <strong>WHAT ABOUT MY VALUABLES?</strong><br/>
                    - Please leave them in your room. The spa will not assume any liabilities.<br/><br/>
                    <strong>SHOULD MEN SHAVE BEFORE THE FACIAL?</strong><br/>
                    - Yes, we recommended that men shave a few hours before their facial.<br/><br/>
                    <strong>WHAT IF I AM LATE FOR AN APPOINTMENT?</strong><br/>
                    - Arriving late will simply limit the time for your treatment, thus lessening its effectiveness and
                    your pleasure. Your treatment will end on time so that the next guest is not delayed.<br/><br/>
                    <strong>WHAT IF I NEED TO MAKE A CANCELLATION?</strong><br/>
                    - A 24-hour cancellation notice is required to help us reschedule your appointment, subject to space
                    availability. Any cancellation with less than 4 hours' notice will incur a 50% cancellation fee.
                    Full charges will be imposed for a "no-show"<br/><br/>
                    <strong>WHAT ABOUT PAYMENT?</strong><br/>
                    - We accept Visa and Master credit cards and cash. You may also charge your treatments to your room. All prices are quoted in Thai Baht. And prices are subject to change without prior notice.
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>

    </div>
</section>


<?php include("../application/views/front/footer.php");?>

