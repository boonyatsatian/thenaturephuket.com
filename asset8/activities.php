<?php include("../application/views/front/meta.php");?>
<?php include("../application/views/front/header.php");?>



<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center">
                    <h1>Activities</h1>
                </div>
                <p>Fill your day with fun for any age and learn some more about Thai culture with our special classes including Thai Cooking; Fruit & Vegetable Carving; and Batik Painting.
                </p>
                <hr/>
            </div>
        </div>

        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Thai Cooking Classes</h2>
                <p>
                    Take home some new south-east Asian culinary skills with Thai cooking classes giving you the skills and know-how to recreate regional dishes from around Siam
                    Classes held every Monday, Wednesday and Friday at midday.
                    Learn how to prepare and cook a 5-course menu of traditional Thai starters, main courses and desserts and earn a Spice Cuisine Certificate.
                </p>
                <h3>These are the dishes you'll learn how to cook.</h3>
                <p>- Spicy Grilled Beef Salad<br/>
                    - Spicy Prawn & Lemon Grass Soup<br/>
                    - Green Curry<br/>
                    - Fried Mixed Vegetable with Oyster Sauce<br/>
                    - Banana & Coconut Milk Dessert</p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/cooking-class/1.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/cooking-class/2.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/cooking-class/3.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/cooking-class/4.jpg" width="100%"></div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Batik Painting Classes</h2>
                <p>
                    Learn how to make your own batik designs and take home your very own batik T-shirt<br/>
                    Enjoy a free drink while you paint and take home your own batik design creations<br/>
                    T-shirt THB 590<br/>
                    Napkin THB 420<br/>
                    Handkerchief THB 350<br/>
                    <strong>Classes held every Tuesday, Thursday and Saturday at 3:30pm.</strong>
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-md-8 col-md-offset-2 ">
                <h2 class="space-between-h2-and-p">Fruit & Vegetable Carving Classes</h2>
                <p>
                    Learn the intricate traditional Thai culinary art of Fruit & Vegetable Carving for beautiful edible displays for table tops and banquets using a special carving knoife, which you can take home with you to practice your new found craft.<br/>
                    <strong>Classes held Tuesdays & Thursdays 3.30pm – 5.00pm</strong>
                </p>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 space-for-img-thump"><img src="images/promotion.jpg" width="100%"></div>
                </div>
            </div>
        </div>


    </div>
</section>


<?php include("../application/views/front/footer.php");?>

