<?php include("../application/views/front/meta.php"); ?>
<?php include("../application/views/front/header.php"); ?>



<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center">
                    <h1>Weddings</h1>
                </div>
                <h2 class="space-between-h2-and-p">Weddings at Aquamarine</h2>

                <p>We specialize in making wedding days truly memorable with dedicated facilities and wedding planners
                    on hand to help you organize your once-in-a-lifetime day in a very special place.</p>

                <h2 class="space-between-h2-and-p">Aquamarine Wedding Packages</h2>

                <p>We specialize in making wedding days truly memorable with dedicated facilities and wedding planners
                    on hand to help you organize your once-in-a-lifetime day in a very special place.</p>


                <hr/>


                <div class="row">
                    <h2 class="space-between-h2-and-p">Western (Christian) Wedding Ceremony</h2>

                    <p>
                        <strong>Special offer from now until December, 2015</strong><br/>
                        - Ceremonial set-up and decoration<br/>
                        - Wedding Solemnization by Master of Ceremony<br/>
                        - Wedding floral decoration in the bedroom<br/>
                        - Aquamarine wedding ceremony certificate<br/>
                        - One flower bouquet for bride<br/>
                        - Corsages for the groom and special guest<br/>
                        - Wedding cake<br/>
                        - One bottle of champagne<br/>
                        - Private Romantic dinner for two, including sparkling wine<br/>
                        - Banquet arrangement with flower decoration<br/>
                        - Bridal SPA Treatment or Treatment for couple
                    </p>

                    <h3 class="space-between-h2-and-p">These extras to make your wedding that more memorable are also
                        available:</h3>

                    <p>- Photography (CD album)<br/>
                        - Wedding Video (DVD Video)<br/>
                        - Makeover and hair styling for both<br/>
                        - Wedding Dinner (venue and menu can be arranged after arrival)
                    </p>

                    <h3 class="space-between-h2-and-p">Things Brides & Grooms Should prepare:</h3>

                    <p>- Wedding dress / attire<br/>
                        - Wedding rings<br/>
                        - Measurements of the Bride & Groom for the wedding costumes must reach the hotel at least four
                        full working days before the wedding day.<br/>
                        - Passports<br/>
                        - Letter or document of certification from bride and groom's embassy(ies) indicating their wish
                        to get married in Thailand, including personal details: names; addresses; nationalities, as well
                        as parent's names and nationalities.<br/>
                        - Divorce document (if previously married)<br/>
                        - All documents need to be translated into Thai Language<br/>
                        - All documents need to be endorsed by the Ministry of Foreign Affairs in Bangkok<br/><br/>
                        *** Please note that the above is not a legally recognized ceremony. Prices are subject to 10%
                        service charge and applicable TAX. ***
                    </p>

                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="clearfix"></div>
                    <br/><br/><br/>
                </div>





                <div class="row">
                    <h2 class="space-between-h2-and-p">Thai Wedding Ceremony</h2>

                    <p>
                        <strong>A traditional Thai wedding ceremony with Buddhist monks to chant a blessing, usually from the auspicious hours of 9:30am until noon.</strong><br/>
                        - Wedding ceremony with five monks<br/>
                        - Lunch and donation for the monks and temple coordinator<br/>
                        - Bouquet for the bride and corsage for the groom<br/>
                        - Garlands for the bride & groom<br/>
                        - Floral gate at entrance, decorations for the blessing ceremony and flowers in the bedroom<br/>
                        - Long drum procession to the venue<br/>
                        - Wedding cake and a bottle of champagne<br/>
                        - Private Romantic Dinner for two with including sparkling wine<br/>
                        - Traditional water pouring ceremony for bride & groom<br/>
                        - Champagne breakfast in bed
                    </p>

                    <h3 class="space-between-h2-and-p">These extras to make your wedding that more memorable are also available:</h3>

                    <p>- Photography (CD album)<br/>
                        - Wedding Video (DVD Video)<br/>
                        - Makeover and hair styling for both<br/>
                        - Wedding Dinner (venue and menu can be arranged after arrival)
                    </p>

                    <h3 class="space-between-h2-and-p">Things for Bride & Groom to prepare:</h3>

                    <p>- Wedding dress / attire<br/>
                        - Wedding rings<br/>
                        - Measurements of the Bride & Groom for the wedding costumes must reach the hotel at least four
                        full working days before the wedding day.<br/>
                        - Passports<br/>
                        - Letter or document of certification from bride and groom's embassy(ies) indicating their wish
                        to get married in Thailand, including personal details: names; addresses; nationalities, as well
                        as parent's names and nationalities.<br/>
                        - Divorce document (if previously married)<br/>
                        - All documents need to be translated into Thai Language<br/>
                        - All documents need to be endorsed by the Ministry of Foreign Affairs in Bangkok<br/><br/>
                        *** Please note that the above is not a legally recognized ceremony. Prices are subject to 10%
                        service charge and applicable TAX. ***
                    </p>

                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="col-lg-3 space-for-promotion"><img src="images/promotion.jpg" width="100%"></div>
                    <div class="clearfix"></div>
                    <br/><br/><br/>
                </div>





            </div>
        </div>
    </div>
</section>


<?php include("../application/views/front/footer.php"); ?>

