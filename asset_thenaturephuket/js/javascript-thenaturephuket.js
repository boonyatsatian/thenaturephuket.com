$(document).ready(function () {
    // $('.loading').animsition();

    $('#maximage').maximage({
        cycleOptions: {
            fx: 'fade',
            speed: 1000,
            timeout: 5000,
            prev: '#arrow_left',
            next: '#arrow_right',
            pager: '#cycle-nav ul',
            pagerAnchorBuilder: function (idx, slide) {
                return '<li><a href="#"></a></li>';
            }
        }, onFirstImageLoaded: function () {
            jQuery('#cycle-loader').hide();
            jQuery('#maximage').fadeIn('fast');
        }
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 80) {
            $(".space-for-header").addClass("scroll_menu");
        } else {
            $(".space-for-header").removeClass("scroll_menu");
        }
    });

    $('#toggle_menu').click(function () {
        $(this).toggleClass('active');
        $('#overlay_popup').toggleClass('open');
        $('body').toggleClass('fix_body');

    });
    $('a[href*=#arrow_down]').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var $target = $(this.hash);
            $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');
            if ($target.length) {
                var targetOffset = $target.offset().top - 80;
                $('html,body').animate({scrollTop: targetOffset}, 1000);
                return false;
            }
        }
    });
    $(function () {
        $('.swipebox').swipebox();
    });




});




$(document).ready(function () {
    $('.slide_offers').owlCarousel({
        loop: true,
        navText: ["<div><i class='fa fa-long-arrow-left' aria-hidden='true'></i> </div>", "<div><i class='fa fa-long-arrow-right' aria-hidden='true'></i></div>"],
        autoplay: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 1000,
        dots: true,
        pagination: false,
        autoplayHoverPause: true,
        margin: 50,
        responsive: {0: {items: 1}, 769: {items: 2}, 1000: {items: 2}, 1200: {items: 3}}
    });
    $('.slide_accommodation').owlCarousel({
        loop: true,
        navText: ["<div><i class='fa fa-long-arrow-left' aria-hidden='true'></i> </div>", "<div><i class='fa fa-long-arrow-right' aria-hidden='true'></i></div>"],
        autoplay: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 1000,
        dots: true,
        pagination: false,
        autoplayHoverPause: true,
        margin: 0,
        responsive: {0: {items: 1}, 769: {items: 1}, 1000: {items: 1}, 1199: {items: 1}}
    });
    $('.slide_facilities').owlCarousel({
        loop: true,
        navText: ["<div><i class='fa fa-long-arrow-left' aria-hidden='true'></i> </div>", "<div><i class='fa fa-long-arrow-right' aria-hidden='true'></i></div>"],
        autoplay: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 1000,
        dots: true,
        pagination: false,
        autoplayHoverPause: true,
        margin: 50,
        responsive: {0: {items: 1}, 769: {items: 1}, 1000: {items: 1}, 1199: {items: 1}}
    });
    $('.slide_contentchild').owlCarousel({
        loop: true,
        navText: ["<div><i class='fa fa-long-arrow-left' aria-hidden='true'></i> </div>", "<div><i class='fa fa-long-arrow-right' aria-hidden='true'></i></div>"],
        autoplay: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 1000,
        dots: true,
        pagination: false,
        autoplayHoverPause: true,
        margin: 50,
        responsive: {0: {items: 1}, 769: {items: 1}, 1000: {items: 1}, 1199: {items: 1}}
    });
    $('.slide_photoinfo').owlCarousel({
        loop: true,
        navText: ["<div><i class='fa fa-long-arrow-left' aria-hidden='true'></i> </div>", "<div><i class='fa fa-long-arrow-right' aria-hidden='true'></i></div>"],
        autoplay: true,
        // animateOut: 'fadeOut',
        // animateIn: 'fadeIn',
        smartSpeed: 1000,
        dots: true,
        pagination: false,
        autoplayHoverPause: true,
        margin: 0,
        responsive: {0: {items: 1}, 769: {items: 2}, 1000: {items: 3}, 1199: {items: 3}}
    });
    $('.slide_hotelfacilities').owlCarousel({
        loop: true,
        navText: ["<div><i class='fa fa-long-arrow-left' aria-hidden='true'></i> </div>", "<div><i class='fa fa-long-arrow-right' aria-hidden='true'></i></div>"],
        autoplay: true,
        // animateOut: 'fadeOut',
        // animateIn: 'fadeIn',
        smartSpeed: 1000,
        dots: true,
        pagination: false,
        autoplayHoverPause: true,
        margin: 50,
        responsive: {0: {items: 1}, 767: {items: 2}, 980: {items: 3}, 1199: {items: 4}}
    });
    $('.slide_popup').owlCarousel({
        loop: true,
        navText: ["<div><i class='fa fa-long-arrow-left' aria-hidden='true'></i> </div>", "<div><i class='fa fa-long-arrow-right' aria-hidden='true'></i></div>"],
        autoplay: true,
        // animateOut: 'fadeOut',
        // animateIn: 'fadeIn',
        smartSpeed: 1000,
        dots: false,
        pagination: false,
        autoplayHoverPause: true,
        margin: 10,
        autoHeight:true,
        responsive: {0: {items: 1}, 767: {items: 1}, 980: {items: 1}, 1199: {items: 1}}
    });

    $('#lang_primary').click(function (e) {
        $('.lang_parent').toggleClass('active');
        e.stopPropagation();
    });
    $(".box_lang").click( function (e) {
        $(".option_lang").toggleClass("active");
        $(".name_lang").toggleClass("active");
        e.stopPropagation();

    });

});



// $(document).ready(function () {
//     $('#gallery').lightGallery({});
// });

// $(document).ready(function () {
//     $(".dropdown").hover(function () {
//         $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideDown("400");
//         $(this).toggleClass('open');
//     }, function () {
//         $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideUp("400");
//         $(this).toggleClass('open');
//     });
// });


$(document).ready(function () {
    $(".collapse.in").each(function () {
        $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
    });
    $(".collapse").on('show.bs.collapse', function () {
        $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hide.bs.collapse', function () {
        $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });
});

/*-------------------------------------*/
/* LANGUAGES */
/*-------------------------------------*/


$(document).click(function(e) {
    if (!$(e.target).is('.lang_parent, .lang_parent *')) {
        $(".lang_parent").removeClass("active");
    }
});

/*-------------------------------------*/
/*LANGUAGE MOBILE */
/*-------------------------------------*/



$(document).click(function(e) {
    if (!$(e.target).is('.base_option, .base_option *')) {
        $(".option_lang").removeClass("active");
        $(".name_lang").removeClass("active");
    }
});