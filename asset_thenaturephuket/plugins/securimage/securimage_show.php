<?php



include 'securimage.php';

$img = new securimage();


$img->image_width = 120;
$img->image_height = 40;
$img->perturbation = 0.85; // 1.0 = high distortion, higher numbers = more distortion
$img->code_length = rand(4,5);
$img->image_bg_color = new Securimage_Color("#ffffff");
$img->use_transparent_text = true;
$img->text_transparency_percentage = 70; // 100 = completely transparent
$img->num_lines = 15;
$img->image_signature = '';
$img->text_color = new Securimage_Color("#000000");
$img->line_color = new Securimage_Color("#cccccc");


$img->show(); // alternate use:  $img->show('/path/to/background_image.jpg');




