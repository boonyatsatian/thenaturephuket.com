<?php

//$info : Page Detail
//$total_page : Total number of pages
//$current_page : Current Page


if (!isset($page_link)) {
    $page_link = $module;
}

if (count($info) > 0) {
    ?>

    <?php

    foreach ($info as $index => $infoArray) {
        if ($index !== 'title') {
            ?>
            <div class="grid-accom">
                <div class="bg-white editable" page-id="<?= $infoArray['page_id'] ?>">
                    <div class="content-p-accom">
                        <div class="hover01 column">
                            <figure>
                                <img src="<?= base_url() . 'images/' . $infoArray['photo'][0]['image_id'] ?>x800x400.jpg"
                                     class="img-responsive editphoto"
                                     photo-id="<?= $infoArray['photo'][0]['photo_id']; ?>" width="100%">
                            </figure>
                        </div>
                        <div class="promotion-accom">
                            <div class="detail-accom">
                                <h3>
                                    <?php

                                    echo $infoArray['title'];

                                    ?>
                                </h3>

                                <?php

                                if (isset($infoArray['short_content'])) {
                                    echo '<p>' . $infoArray['short_content'] . '<p>';
                                }

                                ?>


                                <h5><a href="#" style="text-align:right;"
                                       id="popupModal<?= $infoArray['page_id'] ?>"><?= $this->lang->line('Read More'); ?></a>
                                </h5>
                                <div align="right"><a href="#"
                                                      class="book-now-onpromotion"><?= $this->lang->line('Book Now'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="modalContent<?= $infoArray['page_id'] ?>" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">
                                <?php echo $infoArray['title']; ?>
                            </h4>
                        </div>
                        <div class="modal-body">

                            <?php echo $infoArray['content']; ?>
                            <div class="photo-accom">
                                <div id="owl-accom<?= $infoArray['page_id'] ?>" class="owl-carousel">

                                    <?php

                                    foreach ($infoArray['photo'] as &$contentPhoto) {

                                        echo getImageURL($contentPhoto['image_url'], 800, 400, ' class="img-responsive editphoto" photo-id="' . $contentPhoto['photo_id'] . '"');
                                    }

                                    ?>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <script>
                $("#owl-accom<?=$infoArray['page_id']?>").owlCarousel({
                    autoPlay: 2500,
                    items: 1,
                    itemsDesktop: [1199, 3],
                    itemsDesktopSmall: [979, 3]
                });
                $('#popupModal<?=$infoArray['page_id']?>').click(function () {
                    $('#modalContent<?=$infoArray['page_id']?>').modal('show');
                    return false;
                });
            </script>
            <?php
        }
    }
    ?>


    <?php
}

if ($total_page > 1) {
    ?>
    <section id="rooms" class="list">

        <ul class="pagination">
            <?php
            $p = 1;

            while ($p <= $total_page) {
                ?>
                <li class="<?php if ($p == $current_page) {
                    echo 'active';
                } ?>"><a href="<?= base_url() . $page_link . '?page=' . $p ?>"><?= $p ?></a></li>
                <?php
                $p++;
            }

            ?>
        </ul>
    </section>
    <?php
}

?>




