<?php

//$property : Site Detail
//$site_social : Social List

include('tpl.booking2.php');
?>
<!-- Sidebar -->
<div class="luxen-widget news-widget">
  <div class="title">
    <h5>
      <?=$property['site_name']?>
    </h5>
  </div>
  <p>
    <?php

	if(isset($contentFooter['short_content']))
	{
		echo $contentFooter['short_content'];
	}

	?>
  </p>
</div>
<div class="luxen-widget news-widget">
  <div class="title">
    <h5><?=$this->lang->line('contactus');?></h5>
  </div>
  <ul class="footer-links">
    <li>
      <p><i class="fa fa-map-marker"></i> <?=$property['site_address'];?> </p>
    </li>
    <?php

	if(isset($property['site_phone']))
	{
	?>
    <li>
      <p><i class="fa fa-phone"></i> <?=$property['site_phone'];?> </p>
    </li>
    <?php
	}

	?>
    <li>
      <p><i class="fa fa-envelope"></i> <?=$property['site_email'];?></p>
    </li>
  </ul>
</div>
<div class="luxen-widget news-widget">
  <div class="title">
    <h5>SOCIAL MEDIA</h5>
  </div>
  <ul class="social-links">
    <?php

	foreach($site_social as $index=>$socialArray)
	{

		if(isset($socialArray['value']))
		{
		?>
		<li><a href="<?=$socialArray['value']?>"><i class="fa fa-<?=$socialArray['name'];?>"></i></a></li>
		<?php
		}

	}
	?>
  </ul>
</div>
