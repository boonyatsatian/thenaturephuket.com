<!DOCTYPE html>
<html lang="en" class="no-js">
<head>

    <link rel="shortcut icon" href="../../../asset8/icon.icon" type="image/x-icon"/>
    <link rel="shortcut icon" href="../../../asset8/icon.png" type="image/x-icon"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aquamarine</title>
    <meta name="description" content="Aquamarine"/>
    
    <link rel="stylesheet" type="text/css" href="../../../asset8/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="../../../asset8/css/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="../../../asset8/css/responce.css?v=2">
    <link rel="stylesheet" type="text/css" href="../../../asset8/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../asset8/wd_booking_engine/css/jquery-ui-1.8.2.custom.css">
    <link rel="stylesheet" type="text/css" href="../../../asset8/css/swipebox.css">

	<script type="text/javascript" src="../../../asset8/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../../../asset8/js/jquery-ui.js"></script>
    <script type="text/javascript" src="../../../asset8/wd_booking_engine/js/jquery.lightbox.js"></script>
    <script type="text/javascript" src="../../../asset8/wd_booking_engine/js/booking.js"></script>
    <script type="text/javascript" src="../../../asset8/js/owl.carousel.js"></script>
    <script type="text/javascript" src="../../../asset8/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../../asset8/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../asset8/js/jquery.swipebox.js"></script>

    <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        autoPlay: 3000,
        items : 5,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
      });
	  
	   $("#carousel-example-generic").owlCarousel({
        autoPlay: 1000
      });
	  
	  $("#owl-demo-for-mobile").owlCarousel({
        autoPlay: 3000,
        items : 1,
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [979,1]
      });
    });
    </script>
    
	<script type="text/javascript">
    $( function( $ ) {
        $( '.swipebox' ).swipebox();
    } )( jQuery );
    </script>
    
    <script>
    $(document).ready(function() {
		$("#open").show();
		$("#close").hide();
		$("#book").hide();
		$("#open").click(function(){
			$("#book").show();
			$("#close").show();
			$("#open").hide();
		});
		$("#close").click(function(){
			$("#book").hide();
			$("#close").hide();
			$("#open").show();
		});
		
    });
    </script>
    
    <!-- Booking Mobile -->
    <script type="text/javascript">
    function booked(frmObj, hid){
        var form = document.getElementById(frmObj);
        form.setAttribute("method", "post");
        form.setAttribute("action", "https://secure.widediscovery.com/mobile/apps/");
        
        var HotelhiddenField = document.createElement("input"); 
        HotelhiddenField.setAttribute("type", "hidden");
        HotelhiddenField.setAttribute("id", "hid");
        HotelhiddenField.setAttribute("name", "hid");
        HotelhiddenField.setAttribute("value", hid);
        form.appendChild(HotelhiddenField);
        form.submit();
    }
    </script>
    <!-- End booking Mobile -->
    </head>
    <body>