<?php

//$property : Site Detail
//$languages : Language List
//$currentlang : Current Language
//$submenu : Sub menu list for Accomodation, Facilities, Rates, Offers, Blog, Event

?>
<section class="space-for-header bg-blue">
    <div class="container">
        <div class="row">
            <div class="header-cotn">
                    <div class="nav_lang">
                        <?php

                        if (isset($languages)) {
                            ?>
                            <ul>
                                <li>
                                    <?= $this->lang->line('tel'); ?> : <a href="tel:<?= $property['site_phone']; ?>"><?= $property['site_phone']; ?></a>
                                </li>


                                <li>
                                    <label id="lang_primary">
                                        <span><?=$currentlang['name']; ?></span>
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </label>
                                    <ul class="lang_parent">
                                        <?

                                        foreach ($languages as $index => $languageArray) {
                                            ?>
                                            <li>
                                                <a href="<?= base_url() . 'locale?locale=' . $languageArray['code'] ?>">
                                                    <span class="language-text"><?= $languageArray['name'] ?></span>
                                                </a>
                                            </li>
                                            <?php
                                        }

                                        ?>
                                    </ul>
                                </li>
                            </ul>
                            <?php
                        }

                        ?>
                    </div>
            </div>
            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 logo">
                    <a href="<?= base_url(); ?>">
                        <?= getImageURL($property['image_url'], 193, 143, 'alt="Hotel"') ?>
                    </a>
                <img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/SHA_plus.png">
            </div>
<!--            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 logo"><a class="hidden-lg" href="--><?//= base_url(); ?><!--"><img-->
<!--                            src="--><?//= base_url(); ?><!--asset8/images/logo.jpg" alt=""/></a></div>-->
            <div class="col-lg-10 col-md-12 menu hidden-xs">
                <ul>
                    <li><a href="<?= base_url(); ?>" class="<?php if ($module == "homepage") {echo 'active';} ?>"><?= $this->lang->line('home'); ?></a></li>
<!--                    <li><a href="--><?//= base_url(); ?><!--aboutus" class="--><?php //if ($module == "aboutus") {echo 'active';} ?><!--">--><?//= $this->lang->line('aboutus'); ?><!--</a></li>-->
<!--                    <li><a href="--><?//= base_url(); ?><!--offers" class="--><?php //if ($module == "offers") {echo 'active';} ?><!--">--><?//= $this->lang->line('offers'); ?><!--</a></li>-->
                    <li><a href="<?= base_url(); ?>accomodations" class="<?php if ($module == "accomodations") {echo 'active';} ?>"><?= $this->lang->line('accomodations'); ?></a></li>
                    <li><a href="<?= base_url(); ?>experiences" class="<?php if ($module == "extendlistingpage1") {echo 'active';} ?>"><?= $this->lang->line('experiences'); ?></a></li>
                    <li><a href="<?= base_url(); ?>restaurantsandbars" class="<?php if ($module == "extendlistingpage6") {echo 'active';} ?>"><?= $this->lang->line('restaurantsandbars'); ?></a></li>
                    <!--                    <li><a href="--><? //=base_url(); ?><!--dining">-->
                    <? //= $this->lang->line('dining'); ?><!--</a></li>-->
<!--                    <li><a href="--><?//= base_url(); ?><!--weddings">--><?//= $this->lang->line('weddings'); ?><!--</a></li>-->
<!--                    <li><a href="--><?//= base_url(); ?><!--activities" class="--><?php //if ($module == "extendlistingpage2") {echo 'active';} ?><!--">--><?//= $this->lang->line('activities'); ?><!--</a></li>-->
                    <li><a href="<?= base_url(); ?>facilities" class="<?php if ($module == "facilities") {echo 'active';} ?>"><?= $this->lang->line('facilities'); ?></a></li>
                    <li><a href="<?= base_url(); ?>promotions" class="<?php if ($module == "extendlistingpage2") {echo 'active';} ?>"><?= $this->lang->line('promotions'); ?></a></li>
                    <li><a href="<?= base_url(); ?>packages" class="<?php if ($module == "extendlistingpage3") {echo 'active';} ?>"><?= $this->lang->line('packages'); ?></a></li>
<!--                    <li><a href="--><?//= base_url(); ?><!--activities">--><?//= $this->lang->line('activities'); ?><!--</a></li>-->
                    <li><a href="<?= base_url(); ?>gallery" class="<?php if ($module == "gallery") {echo 'active';} ?>"><?= $this->lang->line('gallery'); ?></a></li>
                    <li><a href="<?= base_url(); ?>location" class="<?php if ($module == "location") {echo 'active';} ?>"><?= $this->lang->line('location'); ?></a></li>
                    <li><a href="<?= base_url(); ?>contactus" class="<?php if ($module == "contactus") {echo 'active';} ?>"><?= $this->lang->line('contactus'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>


<section class="section_social">
    <div class="bg-social">
        <ul>
            <?php
            foreach ($site_social as $index => $socialArray) {

                if ($socialArray['value']) {
                    ?>
                    <li>
                        <a href="<?= $socialArray['value'] ?>" target="_blank"><i class="fa fa-<?= $socialArray['name'] ?>" aria-hidden="true"></i></a>
                    </li>

                    <?php
                }
            }
            ?>
        </ul>
    </div>
</section>



<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
<!--    <div class="carousel-inner" role="listbox">-->
<!---->
<!--        --><?php
//
//        $moduleArray = array('offers', 'accomodations', 'facilities', 'extendlistingpage1', 'extendlistingpage2', 'extendlistingpage3', 'extendlistingpage4');
//
//        if (!isset($slug) && in_array($module, $moduleArray)) {
//            $sql = "SELECT site_page_slide.* ";
//            $sql .= "FROM site_page, site_page_slide \n";
//            $sql .= "WHERE site_page.module = '" . $module . "' ";
//            $sql .= "AND site_page.page_id = site_page_slide.page_id ";
//            $sql .= "ORDER BY site_page_slide.display_order ASC ";
//            $query = $this->db->query($sql);
//            $rs = $query->result_array();
//
//            if (count($rs) > 0) {
//                foreach ($rs as $index => &$slideArray) {
//                    $isActive = ($index == 0) ? 'active' : '';
//                    ?>
<!--                    <div class="item --><?//= $isActive ?><!--">-->
<!--                        <img src="--><?//= base_url(); ?><!--images/--><?//= $slideArray['image_id'] ?><!--x1600x800" width="100%"-->
<!--                             alt="Owl Image">-->
<!--                    </div>-->
<!--                    --><?php
//                }
//            } else {
//                foreach ($defaultSlide as $index => &$slideArray) {
//                    $isActive = ($index == 0) ? 'active' : '';
//                    ?>
<!--                    <div class="item --><?//= $isActive ?><!--">-->
<!--                        <img src="--><?//= base_url(); ?><!--images/--><?//= $slideArray['image_id'] ?><!--x1600x800" width="100%"-->
<!--                             alt="Owl Image">-->
<!--                    </div>-->
<!--                    --><?php
//                }
//            }
//        } else {
//            if (isset($slides) && count($slides) > 0) {
//                foreach ($slides as $index => &$slideArray) {
//                    $isActive = ($index == 0) ? 'active' : '';
//                    ?>
<!--                    <div class="item --><?//= $isActive ?><!--">-->
<!--                        <img src="--><?//= base_url(); ?><!--images/--><?//= $slideArray['image_id'] ?><!--x1600x800" width="100%"-->
<!--                             alt="Owl Image">-->
<!--                    </div>-->
<!--                    --><?php
//                }
//            } else {
//                foreach ($defaultSlide as $index => &$slideArray) {
//                    $isActive = ($index == 0) ? 'active' : '';
//                    ?>
<!--                    <div class="item --><?//= $isActive ?><!--">-->
<!--                        <img src="--><?//= base_url(); ?><!--images/--><?//= $slideArray['image_id'] ?><!--x1600x800" width="100%"-->
<!--                             alt="Owl Image">-->
<!--                    </div>-->
<!--                    --><?php
//                }
//            }
//        }
//        ?>
<!---->
<!--    </div>-->

<!--    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">-->
<!--        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>-->
<!--        <span class="sr-only">Previous</span>-->
<!--    </a>-->
<!--    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">-->
<!--        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>-->
<!--        <span class="sr-only">Next</span>-->
<!--    </a>-->
<!--    --><?php //include('tpl.booking.php') ?>
</div>

<div class="menu-mobile">
    <div class="container">
        <div class="button_container" id="toggle_menu">
            <span class="top"></span>
            <span class="middle"></span>
            <span class="bottom"></span>
        </div>

        <div class="overlay" id="overlay_popup">
            <nav class="overlay-menu">
                <ul>
                    <li class="<?php if ($module == "homepage") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>"><?= $this->lang->line('home'); ?></a>
                    </li>
<!--                    <li class="--><?php //if ($module == "aboutus") {
//                        echo 'active';
//                    } ?><!--">-->
<!--                        <a href="--><?//= base_url() ?><!--">--><?//= $this->lang->line('aboutus'); ?><!--</a>-->
<!--                    </li>-->
                    <li class="<?php if ($module == "accomodations") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>accomodations">
                            <?= $this->lang->line('accomodations'); ?>
                        </a>

                    </li>
                    <li class="<?php if ($module == "extendlistingpage1") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>experiences">
                            <?= $this->lang->line('experiences'); ?>
                        </a>

                    </li>
                    <li class="<?php if ($module == "extendlistingpage6") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>restaurantsandbars">
                            <?= $this->lang->line('restaurantsandbars'); ?>
                        </a>

                    </li>
                    <li class="<?php if ($module == "facilities") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>facilities">
                            <?= $this->lang->line('facilities'); ?>
                        </a>
                    </li>
                    <li class="<?php if ($module == "extendlistingpage2") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>promotions">
                            <?= $this->lang->line('promotions'); ?>
                        </a>

                    </li>
                    <li class="<?php if ($module == "extendlistingpage3") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>packages">
                            <?= $this->lang->line('packages'); ?>
                        </a>

                    </li>

                    <li class="<?php if ($module == "gallery") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>gallery">
                            <?= $this->lang->line('gallery'); ?>
                        </a>
                    </li>
                    <li class="<?php if ($module == "location") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>location">
                            <?= $this->lang->line('location'); ?>
                        </a>
                    </li>
                    <li class="<?php if ($module == "contactus") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>contactus">
                            <?= $this->lang->line('contactus'); ?>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>



