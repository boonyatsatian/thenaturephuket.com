<section class="bg-gray space-for-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sister-hotel space-for-sisterhotel"><a href="http://www.khaolakemeraldresort.com/"><img src="../../../asset8/images/sister-hotel/sister-hotel_01.png" alt=""/></a></div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sister-hotel space-for-sisterhotel"><a href="http://www.diamondcottage.com/"><img src="../../../asset8/images/sister-hotel/sister-hotel_02.png" alt=""/></a></div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sister-hotel space-for-sisterhotel"><a href="http://www.whitesandbluesea.com/"><img src="../../../asset8/images/sister-hotel/sister-hotel_03.png" alt=""/></a></div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sister-hotel space-for-sisterhotel"><a href="http://www.aquamarineresort.com/"><img src="../../../asset8/images/sister-hotel/sister-hotel_04.png" alt=""/></a></div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sister-hotel space-for-sisterhotel"><a href="http://www.khaolakdiamondresort.com/"><img src="../../../asset8/images/sister-hotel/sister-hotel_05.png" alt=""/></a></div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="col-lg-3 col-md-3 col-sm-2 col-xs-3  space-for-sisterhotel"><a href="http://greenleafthai.org/"><img src="../../../asset8/images/sister-hotel/sister-hotel_06.png" alt=""/></a></div>
                <div class="col-lg-3 col-md-3 col-sm-2 col-xs-3  space-for-sisterhotel"><a href="http://www.pata.org/"><img src="../../../asset8/images/sister-hotel/sister-hotel_07.png" alt=""/></a></div>
                <div class="col-lg-3 col-md-3 col-sm-2 col-xs-3  space-for-sisterhotel"><a href="#"><img src="../../../asset8/images/sister-hotel/sister-hotel_08.png" alt=""/></a></div>
                <div class="col-lg-3 col-md-3 col-sm-2 col-xs-3  space-for-sisterhotel"><a href="promotions.php"><img src="../../../asset8/images/sister-hotel/sister-hotel_09.png" alt=""/></a></div>
            </div>
        </div>
    </div>
</section>

<section class="space-for-footer">
    <div class="container text-center">
        <p>Copyright © 2016 Aquamarine Resort & Villa</p>
        <p><a href="http://www.hoteliers.guru/">Hotel Website Design | Online Marketing | Hotel Booking Engine by Hoteliers.guru</a></p>
    </div>
</section>


</body>
</html>