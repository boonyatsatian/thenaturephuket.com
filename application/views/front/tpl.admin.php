<?
$sql = "SELECT COUNT(*) AS num \n";
$sql .= "FROM site_login \n";
$sql .= "WHERE mysession_id = '$_SESSION[mysession_id]' ";
$query = $this->db->query($sql);
$rs = $query->result();

if ($rs[0]->num) {
    ?>
    <style>
        .editable {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            width: 100%;
            /*height: 100%;*/
            position: relative;
        }

        .editable::before {
            content: "Click to edit content";
            text-transform: uppercase;
            letter-spacing: 0.1em;
            font-size: 12px;
            position: relative;
            top: 0;
            font-weight: 100;
            left: 2px;
            padding: 5px 20px;
            background: #f00;
            font-weight: bold;
            color: #fff;
            position: absolute;
        }

        .editable::after {
            content: "";
            display: block;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            border: dashed 2px #f00;
        }

        .editable:hover::after {
            border-color: #971a1a;
        }

        .editable:hover::before {
            background: #971a1a;
        }

        .editphoto {
            position: relative;
        }

        .editphoto::before {
            content: "Click to change photo";
            position: absolute;
            width: 80%;
            height: auto;
            display: block;
            left: 0;
            right: 0;
            top: -14px;
            margin: auto;
            padding: 5px 0px;
            z-index: 900;
            text-transform: uppercase;
            font-size: 12px;
            background: #e1e1e1;
            text-align: center;
            letter-spacing: 0.03em;
            color: #000;
        }

        .admin {

            position: fixed;
            right: 40px!important;
            bottom: 30px;
            z-index: 999;
            color: #fff;
            -webkit-animation: tada 1s;
            -webkit-animation-play-state: play;
            animation: tada 1s;
            animation-play-state: play;
        }

        .admin a.back-btn {
            font-family: Arial, Helvetica, sans-serif !important;
            display: block;
            text-transform: uppercase;
            background: #57acdf;
            color: #fff;
            text-align: center;
            width: auto;
            height: auto;
            padding: 6px 30px 6px 20px;
            letter-spacing: 0.07em;
            font-size: 14px;
            font-weight: 200;
            overflow: hidden;
            position: relative;
            -webkit-box-shadow: 1px 1px 7px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 1px 1px 7px rgba(0, 0, 0, 0.2);
            box-shadow: 1px 1px 7px rgba(0, 0, 0, 0.2);
            border-radius: 10px;
            cursor: pointer;
            z-index: 500;
        }

        .admin a.back-btn:hover {
            background-color: #33739a;
            color: #fff;
        }

        .admin a.hoteliers img {
            height: auto;
            width: 60px;
            position: fixed;
            right: 0px;
            bottom: 10px;
            z-index: 999;
        }

        .admin a.hoteliers:hover img {
            -webkit-animation: tada 0.5s;
            -webkit-animation-play-state: play;
            animation: tada 0.5s;
            animation-play-state: play;
        }

        #content-editor label {
            color: #000;
            margin: 15px 0;
        }

        #content-modal .modal-body {
            width: 50%;

        }

        #content-modal .modal-content {
            text-align: left;

        }

        #content-modal .close {
            font-size: 35px;
            font-weight: 100;

        }
        @media (max-width: 1024px) {
            #content-modal .modal-body {
                width: 80%;
            }
        }  @media (max-width: 414px) {
            #content-modal .modal-body {
                width: 90%;
            }
        }
    </style>
    <div id="content-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div id="content-editor" class="modal-content"></div>
        </div>
    </div>
    <script>
        $('.editable').click(function () {
            $("#content-editor").load('<? echo base_url();?>editcontent?page_id=' + $(this).attr('page-id'));
            $('#content-modal').modal('show');
            return false;
        });
        $(".editphoto").click(function () {
            $("#content-editor").load('<? echo base_url();?>editphoto?photo_id=' + $(this).attr('photo-id'));
            $('#content-modal').modal('show');
            return false;
        });
        $(".editslide").click(function () {
            $("#content-editor").load('<? echo base_url();?>editslide?photo_id=' + $(this).attr('photo-id'));
            $('#content-modal').modal('show');
        });

    </script>
    <div class="admin">
        <a href="http://hoteliers.guru/" class="hoteliers" target="_blank"><img
                    src="<?= base_url(); ?>/asset/images/hoteliers-logo.png" title="Go to Hoteliers.Guru"></a>
        <a href="<? echo base_url(); ?>admin" class="back-btn"> Back to Admin <i class="fa fa-chevron-circle-right"
                                                                                 aria-hidden="true"></i> </a></div>
    <?
}