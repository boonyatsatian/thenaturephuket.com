<?php

//$info : Page Detail
//$photos : All Photos Detail

?>
<div class="breadcrumb breadcrumb-1 pos-center">
  <h1>
    <?=$info['title']?>
  </h1>
</div>
<div class="content"><!-- Content Section -->
  <div class="container">
    <div class="row">
      
      <div class="about-info clearfix"><!-- About Info -->
        <div class="col-lg-8">
          <div class="editable margint30" page-id="<?=$info['page_id']?>">
            <h3><?=$info['title'];?></h3>
            <?=$info['content'];?>
            <?php

			if(count($photos))
			{
			?>
            <table class="table table-striped">
		  	<?php

				foreach($photos as $index=>$photoArray)
				{
				?>
				<tr>
					<td><img src="<?=base_url().'images/'.$photoArray['image_id']?>x100x100"></td>
					<td><?=$photoArray['content'];?></td>
					<td><a class="button btn btn-info" href="<?=base_url().'file?id='.$photoArray['image_id']?>" target="_blank"><?=$this->lang->line('download');?></a></td>
				</tr>
				<?php
				}

				?>
            </table>
            <?php
			}

			?>
          </div>
        </div>
        <div class="col-lg-4 margint30">
          <?php include('tpl.sidebar.php');?>
        </div>
      </div>
    </div>
  </div>
</div>
