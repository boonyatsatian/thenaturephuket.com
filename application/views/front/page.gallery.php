<!-- Sojern Tag v6_js, Pixel Version: 1 -->
<script>
    (function () {

        var params = {};

        /* Please do not modify the below code. */
        var cid = [];
        var paramsArr = [];
        var cidParams = [];
        var pl = document.createElement('script');
        var defaultParams = {"vid": "hot"};
        for (key in defaultParams) {
            params[key] = defaultParams[key];
        }
        ;
        for (key in cidParams) {
            cid.push(params[cidParams[key]]);
        }
        ;
        params.cid = cid.join('|');
        for (key in params) {
            paramsArr.push(key + '=' + encodeURIComponent(params[key]))
        }
        ;
        pl.type = 'text/javascript';
        pl.async = true;
        pl.src = 'https://beacon.sojern.com/pixel/p/190836?f_v=v6_js&p_v=1&' + paramsArr.join('&');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(pl);
    })();
</script>
<!-- End Sojern Tag -->


<section class="section_slideshow">
    <?php

    //$defaultSlide : Photo Slide

    if (count($defaultSlide) > 0) {
        ?>
        <div id="maximage">
            <?php

            foreach ($defaultSlide as $index => $slideArray) {
                ?>
                <div class="mc-image ">
                    <?= getImageURL($slideArray['image_url'], 1920, 920, 'class="editslide" photo-id="' . $slideArray['photo_id'] . '" style="width:100%"') ?>
                    <div class="overlay_slide"></div>

                </div>
                <?php

            }

            ?>

        </div>

        <?php
    }

    ?>
    <div class="section_arrow_slide">
        <a href="" id="arrow_left"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/left_arrow.png"></a>
        <a href="" id="arrow_right"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/right_arrow.png"></a>
    </div>
    <?php include('tpl.booking.php'); ?>
</section>

<section class="section_titlecontentchild">
    <div class="container">
        <h1>
            <?= $this->lang->line('gallery'); ?>
        </h1>
    </div>
</section>

<section class="section_contentchild section_gallery">
    <div class="container">
        <div class="accommodation-img scrollfade">
            <?php

            foreach ($info as $index => $infoArray) {

                if ($index !== 'title') {
                    ?>

                    <h2><?= $infoArray['title'] ?></h2>
                    <?php
                    foreach ($infoArray['photo'] as $photoIndex => $photoArray) {
                        ?>
                        <div class="row_gallery">
                            <div class="col-xs-3">
                                <a href="<?= base_url() . 'images/' . $photoArray['image_id']; ?>x800x600.jpg"
                                   rel="prettyPhoto[gallery1]" class="swipebox" title="View">
                                    <img alt="<?php if (isset($photoArray['title'])) {
                                        echo $photoArray['title'];
                                    } ?>" class="img-responsive"
                                         src="<?= base_url() . 'images/' . $photoArray['image_id']; ?>x360x270.jpg">
                                </a>
                            </div>
                        </div>
                        <?php
                    }

                    ?>
                    <div class="clearfix"></div>
                    <hr>

                    <?php
                }


            }

            ?>


        </div>
    </div>
</section>

<section class="section_titlecontentchild">
    <div class="container">
        <h1>
            <?= $this->lang->line('video'); ?>
        </h1>
    </div>
</section>

<section class="section_contentchild section_gallery">
    <div class="container">
        <div class="accommodation-img scrollfade">

            <div class="row_gallery">
                <div class="col-xs-12" style="padding: 10px;">
                    <video controls autoplay muted>
                        <source src="<?= base_url(); ?>asset_thenaturephuket/video/video-01.mp4" type="video/mp4">
                    </video>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr>

        </div>
    </div>
</section>