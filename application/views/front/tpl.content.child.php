<?php
if($module == 'extendlistingpage1'){
    $module = 'experiences';
}
if($module == 'extendlistingpage2'){
    $module = 'promotions';
}
if($module == 'extendlistingpage3'){
    $module = 'packages';
}
if($module == 'extendlistingpage6'){
    $module = 'restaurantsandbars';
}
if((count($info) > 0)){
    $countData = 0 ;
    foreach ($info as $index => $infoArray) {

        if ($index !== 'title') {
            ?>
            <?php

            if (isset($infoArray['photo'][0]['image_id'])) {
                ?>
                <div class="box_contentchild">

                    <div class="col-xs-6">
                        <div class="box_images">
                            <div class="image_images images_contentchild" style=" background:url('<?= $infoArray['photo'][0]['image_url']; ?>')"></div>
                        </div>

                        <div class="details_contentchild">
                            <h2>
                                <?= $infoArray['title']; ?>
                            </h2>
                            <div class="editable" page-id="<?= $infoArray['page_id'] ?>">
                                <p>
                                    <?php

                                    if (isset($infoArray['short_content'])) {
                                        echo $infoArray['short_content'];
                                    }

                                    ?>
                                </p>

                            </div>
                            <a href="<?= base_url() . $module . '/' . $infoArray['slug']; ?>">
                                <button class="btn_booknow btn_readmore">
                                    <?= $this->lang->line('Read More'); ?>
                                </button>
                            </a>
                        </div>


                    </div>



                </div>

                <?php
                $countData++;

                if ($countData == 2) {
                    $countData = 0;
                    ?>
                    <div class="clearfix"></div>

                    <?php
                }
                ?>

                <?php
            }

            ?>
            <?php
        }

    }
}
