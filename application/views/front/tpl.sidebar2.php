<?
$query = $this->db->get_where('site_page',array('module'=>'accomodations')); 
$offers = $query->result();
?>
<aside class="layout2 visible-lg">
  <div id="scroll"> 
<?
if(count($offers) > 0){
?>  
    <div id="specials" class="list">
      <ul class="slider">

<?
	foreach($offers as $index=>$value){
		$title = json_decode($offers[$index]->title,true);
		$content = json_decode($offers[$index]->content,true);

		if(!isset($title[$locale])){
			$title[$locale] = $title['en_US'];
		}
		
		if(!isset($content[$locale])){
			$content[$locale] = $content['en_US'];
		}
		

		$data_arr = explode("<p>",$content[$locale]);
		array_shift($data_arr);
		
		$url = base_url()."accomodations/".$offers[$index]->slug;

		$sql = "SELECT photo_id, image_id FROM site_page_photo WHERE page_id = '".$offers[$index]->page_id."' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
?>
        <div class="item"> <img alt="" src="<? echo base_url();?>images/<?=$rs[0]->image_id?>x380x250.jpg" width="380" height="250" />
          <div class="details"> <a href="<?=$url?>">
            <div class="title">
              <?=$title[$locale]?>
            </div>
            <p><? if(isset($data_arr[0])){ echo strip_tags($data_arr[0]);}?></p>
            <div class="button"><span data-hover="<?=$this->lang->line('Read More')?>">
              <?=$this->lang->line('Read More')?>
              </span></div>
            </a> </div>
        </div>
<?
	}
?>
      </ul>
      <div class="nav"></div>
    </div>
  </div>
  
<script>
$('.slider').bxSlider({auto: true, mode: 'fade', pager: false, controls: false});
</script>
 
<?
}
?>  
</aside>
