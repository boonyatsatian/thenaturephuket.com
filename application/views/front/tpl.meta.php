<!DOCTYPE html>
<html lang="en" class="no-js">
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KB2627C');</script>
<!-- End Google Tag Manager -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?php echo($module == "homepage" ? $property['site_name'] . ' ' . $property['site_tagline'] : $info["title"] . " " . $property["site_name"]) ?></title>
    <meta name="description" content="<?php echo (isset($info['description'])) ? $info['description'] : $seo['description'] ?>">
    <meta name="keywords" content="<?php echo (isset($info['keywords'])) ? $info['keywords'] : $seo['keywords'] ?>">

    <?= $property['site_googlega']; ?>


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '752291121985331');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=752291121985331&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


    <link rel="shortcut icon" href="<?= base_url(); ?>asset_thenaturephuket/images/icon/favicon.png" type="image/x-icon"/>

    <script type="text/javascript" src="<?= base_url(); ?>asset_thenaturephuket/js/jquery.js"></script>
    <link href="<?= base_url(); ?>asset_thenaturephuket/css/animate.css" rel="stylesheet" type="text/css">


    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset8/css/responce.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset8/css/swipebox.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset8/new_ibe/font-awesome/css/font-awesome.min.css">


    <script type="text/javascript" src="<?= base_url(); ?>asset8/js/jquery.swipebox.js"></script>


    <!-------------
    ##BOOTSTTAP##
    -------------->
    <script type="text/javascript" src="<?= base_url(); ?>asset_thenaturephuket/plugins/bootstrap/js/bootstrap.js?=v2"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset_thenaturephuket/plugins/bootstrap/css/bootstrap.css?=v2">

    <!-------------
    ##IBE2019##
    -------------->
    <script type="text/javascript"
            src="<?= base_url(); ?>asset_thenaturephuket/plugins/ibe_2019/js/booking.js?v=0.1"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="<?= base_url(); ?>asset_thenaturephuket/plugins/ibe_2019/js/t-datepicker.min.js"></script>
    <link href="<?= base_url(); ?>asset_thenaturephuket/plugins/ibe_2019/css/t-datepicker.min.css" rel="stylesheet"
          type="text/css">
    <link href="<?= base_url(); ?>asset_thenaturephuket/plugins/ibe_2019/css/style.css" rel="stylesheet"
          type="text/css">

    <!-------------
    ##OWL-CAROUSEL##
   -------------->
    <script type="text/javascript"
            src="<?= base_url(); ?>asset_thenaturephuket/plugins/owl_carousel/js/owl.carousel.min.js?=v2"></script>
    <link rel="stylesheet" href="<?= base_url(); ?>asset_thenaturephuket/plugins/owl_carousel/css/owl.theme.default.min.css?=v2"
          type="text/css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset_thenaturephuket/plugins/owl_carousel/css/owl.carousel.min.css?=v2"
          type="text/css">

    <script type="text/javascript" src="<?= base_url() ?>asset_thenaturephuket/plugins/video/js/iphone-inline-video.min.js"></script>
    <link rel="stylesheet" href="<?= base_url() ?>asset_thenaturephuket/plugins/video/css/style_video.css" type="text/css">

    <!-------------
    ##MAXIMAGES##
   -------------->
    <script type="text/javascript" src="<?= base_url(); ?>asset_thenaturephuket/plugins/maximage/js/jquery.maximage.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>asset_thenaturephuket/plugins/maximage/js/jquery.cycle.all.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset_thenaturephuket/plugins/maximage/css/jquery.maximage.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset_thenaturephuket/plugins/maximage/css/jquery.maximage.zoom.css">


    <!-------------
    ##THEME-20##
   -------------->
    <script type="text/javascript"
            src="<?= base_url(); ?>asset_thenaturephuket/js/javascript-thenaturephuket.js?=v2"></script>
    <link href="<?= base_url(); ?>asset_thenaturephuket/css/style-thenaturephuket.css?=v2" rel="stylesheet"
          type="text/css">
    <link href="<?= base_url(); ?>asset_thenaturephuket/css/responsive-thenaturephuket.css?=v2" rel="stylesheet"
          type="text/css">

    <?php
//        $tel = explode (",",$property['site_phone']);
    //    $fex = explode (",",$property['site_fax']);
//    $email = explode (",",$property['site_email']);
    ?>

    <style>
        .book_offers:hover, .owl-theme .owl-nav [class*=owl-]:hover, .section_copyright, .btn_apply, .btn_booknow, .bg-social i, .btn_tabreservation, .button_container span, .btn_readmore{
            background: rgba(<?=$rgb?>, 1)!important;
        }
        h3, .book_offers, .nav_lang .lang_parent li a:hover, .overlay ul li a:hover{
            color: rgba(<?=$rgb?>, 1);
        }
		.widget_02 .btnbook_reser, .section_reservationmobile .col-xs-8 {
			background: #fbc23f !important;
		}

    </style>


<!--    <script>-->
<!--        $(document).ready(function () {-->
<!---->
<!---->
<!--            //Select Transtion Type-->
<!--            $("#transitionType").change(function () {-->
<!--                var newValue = $(this).val();-->
<!---->
<!--                //TransitionTypes is owlCarousel inner method.-->
<!--                owl.data("owlCarousel").transitionTypes(newValue);-->
<!---->
<!--                //After change type go to next slide-->
<!--                owl.trigger("owl.next");-->
<!--            });-->
<!--        });-->
<!--    </script>-->


<!--    <script type="text/javascript">-->
<!--        $(function () {-->
<!--            $('.swipebox').swipebox();-->
<!--        });-->
<!--    </script>-->

<!--    <script>-->
<!--        $(document).ready(function () {-->
<!--            $("#open").show();-->
<!--            $("#close").hide();-->
<!--            $("#book").show();-->
<!---->
<!--            $("#open").click(function () {-->
<!--                $("#book").hide();-->
<!--                $("#close").show();-->
<!--                $("#open").hide();-->
<!--            });-->
<!--            $("#close").click(function () {-->
<!--                $("#book").show();-->
<!--                $("#close").hide();-->
<!--                $("#open").show();-->
<!--            });-->
<!---->
<!--        });-->
<!--    </script>-->

</head>

<!--scrollfade-->
<!--<script type="text/javascript">-->
<!---->
<!--    $(function () {-->
<!--        $(window).scroll(function () {-->
<!--            var scrollTop = $(window).scrollTop();-->
<!--            if (scrollTop != 0)-->
<!--                $('.scrollfade').stop().animate({'opacity': '1'}, 100);-->
<!--            else-->
<!--                $('.scrollfade').stop().animate({'opacity': '0.1'}, 100);-->
<!--        });-->
<!--    });-->
<!---->
<!--</script>-->

<body>