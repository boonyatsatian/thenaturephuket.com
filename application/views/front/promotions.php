<?php include("meta.php");?>
<?php include("header.php");?>



<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center">
                    <h1>Promotions</h1>
                    <h2>The Aquamarine Resort & Villa Promotion Special Offer</h2>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-8 col-md-offset-2 "><hr/><div class="clearfix"></div></div>


            <div class="row ">
                <div class="col-md-4 col-sm-4">
                    <div class="col-lg-12">
                        <img src="../../../asset8/images/1.png" width="100%">
                        <br/><br/>
                        <h2 class="space-between-h2-and-p">4 Days 3 Nights Getaway Package</h2>
                        <h3>Validity: 01 April - 31 October 2015</h3>
                        <p><strong>Package inclusive of:-</strong><br/>
                            - 3 nights accommodation in New Deluxe room with daily breakfast<br/>
                            - Round Trip Airport Transfer by Private Car<br/>
                            - Free one time Set dinner per stay<br/>
                            - Free one time 60 minutes Oil Massage at Lotus Spa<br/>
                            - Free one time Phuket City Tour<br/>
                            - Free use of Fitness Centre<br/>
                            - Free Shuttle bus to Kamala Beach and Patong Beach as per hotel schedule<br/>
                            - 20% discount for food only at Hotel outlets (exclude room service and in-house promotion)<br/>
                            - 30% discount for Spa Treatment at Lotus Spa (exclude in-house promotion)
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="col-lg-12">
                        <img src="../../../asset8/images/2.png" width="100%">
                        <br/><br/>
                        <h2 class="space-between-h2-and-p">4 Days 3 Nights Getaway Package</h2>
                        <h3>Validity: 01 April - 31 October 2015</h3>
                        <p><strong>Package inclusive of:-</strong><br/>
                            - 3 nights accommodation in New Deluxe room with daily breakfast<br/>
                            - Round Trip Airport Transfer by Private Car<br/>
                            - Free one time Set dinner per stay<br/>
                            - Free one time 60 minutes Oil Massage at Lotus Spa<br/>
                            - Free one time Phuket City Tour<br/>
                            - Free use of Fitness Centre<br/>
                            - Free Shuttle bus to Kamala Beach and Patong Beach as per hotel schedule<br/>
                            - 20% discount for food only at Hotel outlets (exclude room service and in-house promotion)<br/>
                            - 30% discount for Spa Treatment at Lotus Spa (exclude in-house promotion)
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="col-lg-12">
                        <img src="../../../asset8/images/3.png" width="100%">
                        <br/><br/>
                        <h2 class="space-between-h2-and-p">4 Days 3 Nights Getaway Package</h2>
                        <h3>Validity: 01 April - 31 October 2015</h3>
                        <p><strong>Package inclusive of:-</strong><br/>
                            - 3 nights accommodation in New Deluxe room with daily breakfast<br/>
                            - Round Trip Airport Transfer by Private Car<br/>
                            - Free one time Set dinner per stay<br/>
                            - Free one time 60 minutes Oil Massage at Lotus Spa<br/>
                            - Free one time Phuket City Tour<br/>
                            - Free use of Fitness Centre<br/>
                            - Free Shuttle bus to Kamala Beach and Patong Beach as per hotel schedule<br/>
                            - 20% discount for food only at Hotel outlets (exclude room service and in-house promotion)<br/>
                            - 30% discount for Spa Treatment at Lotus Spa (exclude in-house promotion)
                        </p>
                    </div>
                </div>
            </div>






        </div>
    </div>
</section>


<?php include("footer.php");?>

