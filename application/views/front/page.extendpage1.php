<section class="section_slideshow">
    <?php

    //$defaultSlide : Photo Slide

    if (count($defaultSlide) > 0) {
        ?>
        <div id="maximage">
            <?php

            foreach ($defaultSlide as $index => $slideArray) {
                ?>
                <div class="mc-image ">
                    <?= getImageURL($slideArray['image_url'], 1920, 1000, 'class="editslide" photo-id="' . $slideArray['photo_id'] . '" style="width:100%"') ?>
                    <div class="overlay_slide"></div>
                    <!--                    <div class="text_title_slide">-->
                    <!--                        <ul>-->
                    <!--                            <li>-->
                    <!--                                <h1>--><?//= $property['site_name']; ?><!--</h1>-->
                    <!--                            </li>-->
                    <!--                            <li>-->
                    <!--                                <p>--><?//= $property['site_tagline'] ?><!--</p>-->
                    <!--                            </li>-->
                    <!--                        </ul>-->
                    <!--                    </div>-->

                </div>
                <?php

            }

            ?>

        </div>

        <?php
    }

    ?>
    <div class="section_arrow_slide">
        <a href="" id="arrow_left"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/left_arrow.png"></a>
        <a href="" id="arrow_right"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/right_arrow.png"></a>
    </div>

    <?php include('tpl.booking.php'); ?>

</section>

<section class="section_titlecontentchild box-entity ">
    <div class="container">
        <h1>
            <?= $this->lang->line('sitemap'); ?>
        </h1>
        <div class="section_sitemapmain">
            <div class="col-xs-6">
                <ul>
                    <li class=" <?php if ($module == "homepage") {
                        echo 'active';
                    } ?>"><a href="<?= base_url(); ?>"><?= $this->lang->line('home'); ?></a></li>

<!--                    <li class=" --><?php //if ($module == "aboutus") {
//                        echo 'active';
//                    } ?><!--">-->
<!--                        <a href="--><?//= base_url() ?><!--aboutus">-->
<!--                            --><?//= $this->lang->line('aboutus'); ?>
<!--                        </a>-->
<!--                    </li>-->


<!--                    <li class=" --><?php //if ($module == "offers") {
//                        echo 'active';
//                    } ?><!--">-->
<!--                        <a href="--><?//= base_url() ?><!--offers" onclick="window.location.href=this.href">-->
<!--                            --><?//= $this->lang->line('offers'); ?>
<!--                        </a>-->
<!--                        --><?php
//
//                        if (isset($submenu['offers']['menu'])) {
//                            ?>
<!--                            <ul class="sub_menusitemap">-->
<!--                                --><?php
//
//                                foreach ($submenu['offers']['menu'] as $index => $value) {
//                                    ?>
<!--                                    <li>-->
<!--                                        <a href="--><?//= base_url() ?><!--offers/--><?//= $submenu['offers']['slug'][$index]; ?><!--">--><?//= $value; ?><!--</a>-->
<!--                                    </li>-->
<!--                                    --><?php
//                                }
//
//                                ?>
<!--                            </ul>-->
<!--                            --><?php
//                        }
//
//                        ?>
<!--                    </li>-->

                    <li class=" <?php if ($module == "accomodations") {
                        echo 'active';
                    } ?>"><a href="<?= base_url() ?>accommodations" onclick="window.location.href=this.href"><?= $this->lang->line('accomodations'); ?></a>
                        <?php

                        if (isset($submenu['accomodations']['menu'])) {
                            ?>
                            <ul class="sub_menusitemap">
                                <?php

                                foreach ($submenu['accomodations']['menu'] as $index => $value) {
                                    ?>
                                    <li>
                                        <a href="<?= base_url() ?>accommodations/<?= $submenu['accomodations']['slug'][$index]; ?>"><?= $value; ?></a>
                                    </li>
                                    <?php
                                }

                                ?>
                            </ul>
                            <?php
                        }

                        ?>
                    </li>
<!--                    <li class=" --><?php //if ($module == "extendlistingpage2") {
//                        echo 'active';
//                    } ?><!--">-->
<!--                        <a href="--><?//= base_url() ?><!--activities">-->
<!--                            --><?//= $this->lang->line('activities'); ?>
<!--                        </a>-->
<!--                    </li>-->

                    <li class="<?php if ($module == "extendlistingpage1") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>experiences"><?= $this->lang->line('experiences'); ?></a>

                        <ul class="sub_menusitemap">
                            <?php

                            $sql = "SELECT * ";
                            $sql .= "FROM site_page \n";
                            $sql .= "WHERE module = 'extendlistingpage1' ";
                            $sql .= "ORDER BY display_order ASC ";
                            $query = $this->db->query($sql);
                            $rs = $query->result();
                            if (count($rs)) {

                                foreach ($rs as $index => $menuArray) {
                                    $menu = json_decode($menuArray->menu, true);

                                    if (!isset($menu[$currentlang['code']]) || $menu[$currentlang['code']] == ''){
                                        $menu[$currentlang['code']] = $menu['en_US'];
                                    }


                                    ?>
                                    <li>
                                        <a href="<?= base_url() . 'experiences/' . $menuArray->slug; ?>">
                                            <?= $menu[$currentlang['code']]; ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }

                            ?>
                        </ul>

                    </li>
                    <li class=" <?php if ($module == "facilities") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>facilities" onclick="window.location.href=this.href">
                            <?= $this->lang->line('facilities'); ?>
                        </a>
                        <?php

                        if (isset($submenu['facilities']['menu'])) {
                            ?>
                            <ul class="sub_menusitemap">
                                <?php

                                foreach ($submenu['facilities']['menu'] as $index => $value) {
                                    ?>
                                    <li>
                                        <a href="<?= base_url() ?>facilities/<?= $submenu['facilities']['slug'][$index]; ?>"><?= $value; ?></a>
                                    </li>
                                    <?php
                                }

                                ?>
                            </ul>
                            <?php
                        }

                        ?>
                    </li>

                </ul>
            </div>
            <div class="col-xs-6">
                <ul>
                    <li class="<?php if ($module == "extendlistingpage2") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>promotions"><?= $this->lang->line('promotions'); ?></a>

                        <ul class="sub_menusitemap">
                            <?php

                            $sql = "SELECT * ";
                            $sql .= "FROM site_page \n";
                            $sql .= "WHERE module = 'extendlistingpage2' ";
                            $sql .= "ORDER BY display_order ASC ";
                            $query = $this->db->query($sql);
                            $rs = $query->result();
                            if (count($rs)) {

                                foreach ($rs as $index => $menuArray) {
                                    $menu = json_decode($menuArray->menu, true);

                                    if (!isset($menu[$currentlang['code']]) || $menu[$currentlang['code']] == ''){
                                        $menu[$currentlang['code']] = $menu['en_US'];
                                    }


                                    ?>
                                    <li>
                                        <a href="<?= base_url() . 'promotions/' . $menuArray->slug; ?>">
                                            <?= $menu[$currentlang['code']]; ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }

                            ?>
                        </ul>

                    </li>
                    <li class="<?php if ($module == "extendlistingpage3") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>packages"><?= $this->lang->line('packages'); ?></a>

                        <ul class="sub_menusitemap">
                            <?php

                            $sql = "SELECT * ";
                            $sql .= "FROM site_page \n";
                            $sql .= "WHERE module = 'extendlistingpage3' ";
                            $sql .= "ORDER BY display_order ASC ";
                            $query = $this->db->query($sql);
                            $rs = $query->result();
                            if (count($rs)) {

                                foreach ($rs as $index => $menuArray) {
                                    $menu = json_decode($menuArray->menu, true);

                                    if (!isset($menu[$currentlang['code']]) || $menu[$currentlang['code']] == ''){
                                        $menu[$currentlang['code']] = $menu['en_US'];
                                    }


                                    ?>
                                    <li>
                                        <a href="<?= base_url() . 'packages/' . $menuArray->slug; ?>">
                                            <?= $menu[$currentlang['code']]; ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }

                            ?>
                        </ul>

                    </li>


                    <!--                    <li class=" --><?php //if ($module == "blog") {
//                        echo 'active';
//                    } ?><!--">-->
<!--                        <a href="--><?//= base_url() ?><!--blog" onclick="window.location.href=this.href">-->
<!--                            --><?//= $this->lang->line('blog'); ?>
<!---->
<!--                        </a>-->
<!--                    </li>-->
                    <li class=" <?php if ($module == "gallery") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>gallery">
                            <?= $this->lang->line('gallery'); ?>
                        </a>
                    </li>
                    <li class=" <?php if ($module == "location") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>location">
                            <?= $this->lang->line('location'); ?>
                        </a>
                    </li>
                    <li class=" <?php if ($module == "contactus") {
                        echo 'active';
                    } ?>">
                        <a href="<?= base_url() ?>contactus">
                            <?= $this->lang->line('contactus'); ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>




