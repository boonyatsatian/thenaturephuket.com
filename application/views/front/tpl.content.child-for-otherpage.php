<?php

//$info : Page Detail
//$total_page : Total number of pages
//$current_page : Current Page

if(!isset($page_link))
{
    $page_link = $module;
}

if(count($info) > 0)
{
    ?>

    <?php

    foreach($info as $index=>$infoArray)
    {
        if($index !== 'title')
        {
            ?>
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="space-between-h2-and-p">
                        <a href="<?=base_url().$page_link.'/'.$infoArray['slug']?>">
                            <?php
                            echo $infoArray['title'];
                            ?>
                        </a>
                    </h2>
                    <div class="editable" page-id="<?=$infoArray['page_id']?>">
                        <p>
                            <?php echo $infoArray['content'];?>
                        </p>
                    </div>
                </div>
                <div class=" accommodation-img">
                    <?php

                    for($numPhoto = 0; $numPhoto < 20; $numPhoto++)
                    {
                        if(isset($infoArray['photo'][$numPhoto]))
                        {
                            ?>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                <a href="<?=base_url().'images/'.$infoArray['photo'][$numPhoto]['image_id']?>x800x600.jpg"  rel="View" class="swipebox" title="View">
                                    <img src="<?=base_url().'images/'.$infoArray['photo'][$numPhoto]['image_id']?>x800x600.jpg" class="img-responsive editphoto" photo-id="<?=$infoArray['photo'][$numPhoto]['photo_id'];?>" width="100%" />
                                </a>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>

            </div>
        <?php
        }
    }
    ?>

<?php
}

if($total_page > 1)
{
    ?>
    <section id="rooms" class="list">

        <ul class="pagination">
            <?php
            $p = 1;

            while($p <= $total_page)
            {
                ?>
                <li class="<?php if($p == $current_page){ echo 'active'; } ?>"><a href="<?=base_url().$page_link.'?page='.$p?>"><?=$p?></a></li>
                <?php
                $p++;
            }

            ?>
        </ul>
    </section>
<?php
}

?>




