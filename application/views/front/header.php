<section class="space-for-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 logo"> <img src="../../../asset8/images/logo.jpg" alt=""/></div>
            <div class="col-lg-10 col-md-12 menu hidden-xs">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="promotions.php">Promotions</a></li>
                    <li><a href="accommodation.php">Rooms</a></li>
<!--                    <li><a href="../../../asset8/dining.php">Dining</a></li>-->
                    <li><a href="../../../asset8/weddings.php">Weddings</a></li>
                    <li><a href="../../../asset8/facilities.php">Facilities</a></li>
                    <li><a href="../../../asset8/spa.php">Spa</a></li>
                    <li><a href="../../../asset8/activities.php">Activities</a></li>
                    <li><a href="gallery.php">Gallery</a></li>
                    <li><a href="../../../asset8/location.php">Location</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<div class="row">
    <div class="navwrapper hidden-sm hidden-md hidden-lg" >
    <div class="navbar navbar-inverse navbar-static-top">
    <div class="container">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand hidden-md  hidden-sm" href="index.php">MENU</a>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li class="menuItem active"><a href="index.php">Home</a></li>
            <li class="menuItem"><a href="promotions.php">Promotions</a></li>
            <li class="menuItem"><a href="accommodation.php">Rooms</a></li>
<!--            <li class="menuItem"><a href="../../../asset8/dining.php">Dining</a></li>-->
            <li class="menuItem"><a href="../../../asset8/weddings.php">Weddings</a></li>
            <li class="menuItem"><a href="../../../asset8/facilities.php">Facilities</a></li>
            <li class="menuItem"><a href="../../../asset8/spa.php">Spa</a></li>
            <li class="menuItem"><a href="../../../asset8/activities.php">Activities</a></li>
            <li class="menuItem"><a href="gallery.php">Gallery</a></li>
            <li class="menuItem"><a href="../../../asset8/location.php">Location</a></li>
        </ul>
    </div>
    </div>
    </div> 
    </div>
</div>

<div class="row" style="position:relative;">
    <div id="owl-demo1" class="owl-carousel">
        <div class="item"><img src="../../../asset8/images/slide1.jpg" width="100%" alt="Owl Image"></div>
        <div class="item"><img src="../../../asset8/images/slide2.jpg" width="100%" alt="Owl Image"></div>
        <div class="item"><img src="../../../asset8/images/slide3.jpg" width="100%" alt="Owl Image"></div>
        <div class="item"><img src="../../../asset8/images/slide4.jpg" width="100%" alt="Owl Image"></div>
    </div>
    
    
    <div id="book" class="hidden-xs hidden-sm" style="position:absolute; bottom:100px; width:100%; z-index:2;">
        <div class="container" style="background:#fff;">
            <div class="cover-input-reservation" style="padding:20px;">
            
          
                <form id="searchavailable" name="searchavailable"></form>
                <span class="text_reser">Check in : </span>
                <input type="text" id="indate" name="indate" class="date-input" value=""/>
                <span class="text_reser">Check out : </span>
                <input type="text" id="outdate" name="outdate" class="date-input" value=""/>
                <span class="text_reser">Rooms : </span>
                <select id="chknoofroom" name="chknoofroom" class="select-input">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
                <span class="text_reser">Adults : </span>
                <select id="chknoadult" name="chknoadult" class="select-input">
                    <option value="1">1
                    </option
                            >
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                </select>
                <span class="text_reser">Children : </span>
                <select id="chknochild" name="chknochild" class="select-input">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                </select>
                <input type="submit" onclick="available('searchavailable');" value="BOOK NOW" class="date-input no-bg"/>
            </div>
        </div>
    </div>
    
    

    <div class="text-center reservation hidden-xs hidden-sm" id="open"><img src="../../../asset8/images/reservation.png" alt=""/></div>
    <div class="text-center reservation hidden-xs hidden-sm" id="close"><img src="../../../asset8/images/reservation.png" alt=""/></div>
    <div class="text-center reservation hidden-lg hidden-md" ><a href="https://secure.widediscovery.com/mobile/apps/"><img src="../../../asset8/images/reservation.png" alt=""/></a></div>
</div>