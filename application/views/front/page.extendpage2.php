<div class="uk-clearfix"></div>

<?php

$sql = "SELECT * \n";
$sql .= "FROM site_page \n";
$sql .= "WHERE module = 'extendpage2' \n";
$sql .= "ORDER BY display_order DESC";
$query = $this->db->query($sql);
$rs = $query->result();


foreach ($rs as $index => $data) {

    $facilitiesintro_id = $data->page_id;
    $data->menu = json_decode($data->menu, true);
    $data->title = json_decode($data->title, true);
    $data->content = json_decode($data->content, true);

    if(!isset($data->menu[$currentlang['code']]) || $data->menu[$currentlang['code']] == '')
        $data->menu[$currentlang['code']] = $data->menu['en_US'];

    if(!isset($data->content[$currentlang['code']]) || $data->content[$currentlang['code']] == '')
        $data->content[$currentlang['code']] = $data->content['en_US'];

    if(!isset($data->title[$currentlang['code']]) || $data->title[$currentlang['code']] == '')
        $data->title[$currentlang['code']] = $data->title['en_US'];

    $splitContent = explode('</p>', $data->content[$currentlang['code']]);
    $shortContent = strip_tags($splitContent[0]);

    ?>
    <div class="editable" page-id="<?= $facilitiesintro_id?>">
        <?= $data->content[$currentlang['code']] ?>
    </div>
    <?
}

?>
