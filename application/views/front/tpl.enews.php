<!-- Form E-Newsletter -->
<div id="dialog_enew" name='dialog_enew' title="Join our mailing list" style="display:none" >
	<form id="form_addnewsletter" name="form_addnewsletter" method="post" action="" enctype="multipart/form-data"  target="_blank">
		<input type='hidden' name="listid" id="listid" value="154" />

		<div align="center" style="margin:0px auto; position:relative;">
			<img src="images/logo_bauman.png" width="137" height="89" border="0" />
		</div>

		<div class="fontBody" style="width:100%;padding-top:5px;padding-bottom:10px;'">
			Sign up for our E-Newsletters to keep up to date with our latest package and news
		</div>

		<div class="fontBody" >
			<span style="color: #FF0000;">*</span> Email&nbsp;&nbsp;&nbsp;:
			<input type="text" id="email" name="email" value="" size='10' style="width:253px;"/>
		</div>

		<div class="fontBody">
			<span style="color: #FF0000;">*</span>
			Name&nbsp;&nbsp;:
			<select id="titlename" name="titlename">
				<option value="Mr.">Mr.</option>
				<option value="Ms.">Ms.</option>
				<option value="Mrs.">Mrs.</option>
				<option value="Miss">Miss</option>
			</select>
			<input type="text" id="firstname" name="firstname" value="" size='50' style="width:100px;"/>
			<input type="text" id="lastname" name="lastname" value="" size='50' style="width:100px;"/>
		</div>

		<div class="fontBody" style="padding-left:100px;padding-top:10px;">
			<img id="captcha" src="<?=base_url()?>/asset/signup/securimage/securimage_show.php" alt="CAPTCHA Image"  style="border: 1px solid;"/>
			<a href="#" onclick="document.getElementById('captcha').src = '<?=base_url()?>/asset/signup/securimage/securimage_show.php?' + Math.random(); return false">
				<img id="xxx" src="<?=base_url()?>/asset/signup/securimage/refresh.gif" border="0"/>
			</a>
		</div>

		<div class="fontBody">
			<span style="color: #FF0000;">*</span>
			Security Code: <input type="text" name="captcha_code" id='captcha_code' size="18" maxlength="6" />
		</div>

		<div style="margin:0px auto; padding-top:20px; text-align:center">
			<a href="http://www.facebook.com/baumancasa" rel="nofollow" target="_blank">
				<img src="images/logo_facebook.png" width="22" height="22" border="0" style="display:inline" />
			</a>
			<a href="http://www.tripadvisor.com/Hotel_Review-g1215780-d2039310-Reviews-Baumancasa_Karon_Beach_Resort-Karon_Phuket.html" rel="nofollow" target="_blank">
				<img src="images/logo_trip.png" width="22" height="22" border="0" style="display:inline" />
			</a>
			<a href="http://twitter.com/#!/baumancasa" rel="nofollow" target="_blank">
				<img src="images/logo_twitter.png" width="22" height="22" border="0" style="display:inline" />
			</a>
			<a href="http://www.youtube.com/user/baumancasa" rel="nofollow" target="_blank">
				<img src="images/logo_youtube.png" width="22" height="22" border="0" style="display:inline" />
			</a>
			<a href="http://www.flickr.com/photos/baumancasa/" rel="nofollow" target="_blank">
				<img src="images/logo_flickr.png" width="22" height="22" border="0" style="display:inline" />
			</a>
			<!-- Place this tag where you want the badge to render -->
			<a href="https://plus.google.com/109070868276130891433?prsrc=3" style="text-decoration:none;" target="_blank">
				<img src="https://ssl.gstatic.com/images/icons/gplus-32.png" alt="" style="border:0;width:22px;height:22px;"/>
			</a>
		</div>

	</form>
</div>

<div id="dialog_enew_thank" name='dialog_enew_thank' title="Thank you" style="display:none" >
	<div align="center" style="margin:0px auto; position:relative;">
		<img src="images/logo_bauman.png" width="137" height="89" border="0" />
	</div>
	<div class="fontBody" style="width:100%;padding-top:5px;padding-bottom:10px;text-align:center;'">
		Thanks for subscribing to our E-Newsletter.
	</div>
</div>
<!-- Form E-Newsletter -->