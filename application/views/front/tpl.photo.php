<?php

//$photos : All Photos Detail

if(count($photos) > 0)
{
?>
<div class="about-services margint60 clearfix"><!-- About Services -->
	<?php

	foreach($photos as $index=>$photoArray)
	{
	?>  
	<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 gallery">
		<a href="<?=base_url().'images/'.$photoArray['image_id'];?>x800x600.jpg" rel="prettyPhoto[gallery]">
	  		<img class="img-responsive editphoto" photo-id="<?=$photoArray['photo_id']?>" src="<?=base_url().'images/'.$photoArray['image_id']?>x360x270" >
		</a>
		<?php

		if(isset($photoArray['title']))
		{
		?>
		<h5 class="margint20"><?=$photoArray['title']?></h5>
		<?php
		}

		if(isset($photoArray['content']))
		{
		?>
		<p class="margint20"><?=$photoArray['content'];?></p>
		<?php
		}

		?>
	</div>
	<?php
	}

	?>
</div>
<?php
}

/*

if(count($photo) > 0){
?>
  <div class="row">
<?
foreach($photo as $index=>$value){?>  
    <div class="col-sm-4">
    <img src="<? echo base_url();?>images/<?=$photo[$index]->image_id?>x400x200" class="editphoto img-responsive" photo-id=<?=$photo[$index]->photo_id;?>>
    </div>
<?
}
?>
  </div>
<br>
<br>

<?
}

*/
?>
