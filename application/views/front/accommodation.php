<?php include("meta.php");?>
<?php include("header.php");?>



<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center">
                    <h1>Accommodation</h1>
                    <h2>The Aquamarine Resort & Villa Promotion Special Offer</h2><br/><br/>
                </div>
                <p>Endless Magical Moments at The Aquamarine Resort & Villas
                    <br/><br/>
                    Experience the heavenly pleasures of authentic Thai hospitality high above Kamala Beach with a
                    magnificent vista of the
                    emerald Andaman Sea on the west coast of Phuket, Thailand, where you'll find all the ingredients for
                    most memorable tropical romance.
                    <br/><br/>
                    Romance is always in the air at this ancient Ayudhya-themed sea view retreat with three exotic
                    swimming pools,
                    a spa and private villas nestled among the ancient trees and beautiful ponds of an unspoiled
                    hillside jungle.
                </p>
                <hr/>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 space-for-promotion">
                <img src="../../../asset8/images/superior-room/1.jpg" width="100%"><br/><br/>
                <h2 class="space-between-h2-and-p">Superior Room</h2>
                <p>Experience the heavenly pleasures of authentic Thai hospitality high above Kamala Beach with a
                    magnificent vista of the
                    emerald Andaman Sea on the west coast of Phuket, Thailand, where you'll find all the ingredients for
                    most memorable tropical romance.</p>
                <div class="row accommodation-img">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="../../../asset8/images/superior-room/1.jpg" rel="View" class="swipebox" title="View"><img src="../../../asset8/images/superior-room/1.jpg" width="100%"></a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="../../../asset8/images/superior-room/2.jpg" rel="View" class="swipebox" title="View"><img src="../../../asset8/images/superior-room/2.jpg" width="100%"></a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="../../../asset8/images/superior-room/3.jpg" rel="View" class="swipebox" title="View"><img src="../../../asset8/images/superior-room/3.jpg" width="100%"></a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="../../../asset8/images/superior-room/4.jpg" rel="View" class="swipebox" title="View"><img src="../../../asset8/images/superior-room/4.jpg" width="100%"></a>
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-md-6 col-sm-6 space-for-promotion">
                <img src="../../../asset8/images/villa-room/1.jpg" width="100%"><br/><br/>
                <h2 class="space-between-h2-and-p">Villa Room</h2>
                <p>Experience the heavenly pleasures of authentic Thai hospitality high above Kamala Beach with a
                    magnificent vista of the
                    emerald Andaman Sea on the west coast of Phuket, Thailand, where you'll find all the ingredients for
                    most memorable tropical romance.</p>
                <div class="row accommodation-img">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="../../../asset8/images/villa-room/1.jpg" rel="View" class="swipebox" title="View"><img src="../../../asset8/images/villa-room/1.jpg" width="100%"></a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="../../../asset8/images/villa-room/2.jpg" rel="View" class="swipebox" title="View"><img src="../../../asset8/images/villa-room/2.jpg" width="100%"></a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="../../../asset8/images/villa-room/3.jpg" rel="View" class="swipebox" title="View"><img src="../../../asset8/images/villa-room/3.jpg" width="100%"></a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="../../../asset8/images/villa-room/4.jpg" rel="View" class="swipebox" title="View"><img src="../../../asset8/images/villa-room/4.jpg" width="100%"></a>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>


<?php include("footer.php");?>

