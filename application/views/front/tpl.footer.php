<?php

//$property : Site Detail
//$site_social : Social List

?>

<section class="section_copyright">
    <div class="container">
        <div class="">
            <div class="sisterhotel">
                <a href="https://www.thecharmresortphuket.com/" target="_blank">
                    <img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/sisterhotel.jpg">
                </a>
            </div>

            <p>
                <i class="fa fa-copyright" aria-hidden="true"></i> <?= date("Y"); ?> <?= $property['site_name']; ?>
                All Rights Reserved <br>Hotel CMS Website | Hotel Booking Engine by <a href="http://www.hoteliers.guru" target="_blank" class="link_hg">Hoteliers.Guru</a> | <a href="<?=base_url()?>site-map"><?= $this->lang->line('sitemap'); ?></a>
            </p>
        </div>
    </div>
</section>

<!--<section class="space-for-footer bg-blue">-->
<!--    <div class="container" align="center">-->
<!--        <div class="row" align="center">-->
<!--            <div class="col-lg-12" align="center">-->
<!--                <div class="partnerimg-b" align="center" ><a href="#"><img src="--><?//=base_url();?><!--asset8/images/imgnew/award1.jpg" alt=""/></a>-->
<!--               <a href="#"><img src="--><?//=base_url();?><!--asset8/images/imgnew/award2.jpg" alt=""/></a>-->
<!--                <a href="#"><img src="--><?//=base_url();?><!--asset8/images/imgnew/award3.jpg" alt=""/></a></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<!---->
<!--<div class="clearfix"></div>-->
<!--<section class="space-for-footer" >-->
<!--    <div class="container text-center">-->
<!--        <p>Copyright © 2015 Luxury Hotel</p>-->
<!--        <p><a style="color:#555;" href="http://www.hoteliers.guru/">Hotel Website Design | Online Marketing | Hotel Booking Engine by Hoteliers.guru</a></p>-->
<!--    </div>-->
<!--</section>-->

<?php include("tpl.admin.php") ?>
</body>
</html>