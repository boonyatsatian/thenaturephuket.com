<div class="breadcrumb breadcrumb-1 pos-center">
  <h1><?=$this->lang->line('event');?></h1>
</div>
<div class="content"><!-- Content Section -->
  <div class="container margint60">
    <div class="row">
      <div class="col-lg-9">
        <?php include("tpl.content.child.php");?>
      </div>
      <div class="col-lg-3"><!-- Sidebar -->
        <?php include('tpl.left.php')?>
      </div>
    </div>
  </div>
</div>
