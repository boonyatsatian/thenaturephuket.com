<!-- Sojern Tag v6_js, Pixel Version: 1 -->
<script>
    (function () {

        var params = {};

        /* Please do not modify the below code. */
        var cid = [];
        var paramsArr = [];
        var cidParams = [];
        var pl = document.createElement('script');
        var defaultParams = {"vid":"hot"};
        for(key in defaultParams) { params[key] = defaultParams[key]; };
        for(key in cidParams) { cid.push(params[cidParams[key]]); };
        params.cid = cid.join('|');
        for(key in params) { paramsArr.push(key + '=' + encodeURIComponent(params[key])) };
        pl.type = 'text/javascript';
        pl.async = true;
        pl.src = 'https://beacon.sojern.com/pixel/p/190836?f_v=v6_js&p_v=1&' + paramsArr.join('&');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(pl);
    })();
</script>
<!-- End Sojern Tag -->


<?php

//$info : Page Detail
//$sendmail : Status for Send contact

?>

<?php

//$info : Page Detail
//$property : Site Detail

?>
<section class="section_slideshow">
    <?php

    //$defaultSlide : Photo Slide

    if (count($slides) > 0) {
        ?>
        <div id="maximage">
            <?php

            foreach ($slides as $index => $slideArray) {
                ?>

                <div class="mc-image ">
                    <?= getImageURL($slideArray['image_url'], 1920, 920, 'class="editslide" photo-id="' . $slideArray['photo_id'] . '" style="width:100%"') ?>
                    <div class="overlay_slide"></div>
                </div>
                <?php

            }

            ?>

        </div>

        <?php
    }

    ?>
    <div class="section_arrow_slide">
        <a href="" id="arrow_left"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/left_arrow.png"></a>
        <a href="" id="arrow_right"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/right_arrow.png"></a>
    </div>
    <?php include('tpl.booking.php'); ?>
</section>

<section class="section_titlecontentchild">
    <div class="container">
        <h1>
            <?= $this->lang->line('contactus'); ?>
        </h1>
        <div class="editable" page-id="<?= $info['page_id'] ?>">
            <?php if (isset($info['content'])) {
                echo $info['content'];
            } ?>
        </div>
        <input type="button" value="<?= $this->lang->line('contactus'); ?>" class="btn_booknow btn_readmore"
               onclick="openWindows('87')">
        <script>
            function openWindows(idHotel, name_hotel) {
                const host = 'https://cms.hoteliers.guru/all-teamweb/module/SendMail/App/View/re-check.php';
                reCheck = window.open(
                    host + '?hotel=' + idHotel + '&name_hotel=' + name_hotel,
                    'popupWindow',
                    'width=570, height=700,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=no'
                );
            }
        </script>
    </div>
</section>

<div class="google_map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.80708952828!2d98.2907592514077!3d7.915209894270177!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30503a9a02267ab5%3A0x1799341a7b6cb38e!2sThe%20Nature%20Phuket!5e0!3m2!1sen!2sth!4v1587974832490!5m2!1sen!2sth" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
