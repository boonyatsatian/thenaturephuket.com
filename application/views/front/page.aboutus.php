<section class="section_slideshow">
    <?php

    //$defaultSlide : Photo Slide

    if (count($slides) > 0) {
        ?>
        <div id="maximage">
            <?php

            foreach ($slides as $index => $slideArray) {
                ?>

                <div class="mc-image ">
                    <?= getImageURL($slideArray['image_url'], 1920, 920, 'class="editslide" photo-id="' . $slideArray['photo_id'] . '" style="width:100%"') ?>
                    <div class="overlay_slide"></div>
                </div>
                <?php

            }

            ?>

        </div>

        <?php
    }

    ?>
    <div class="section_arrow_slide">
        <a href="" id="arrow_left"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/left_arrow.png"></a>
        <a href="" id="arrow_right"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/right_arrow.png"></a>
    </div>
    <?php include('tpl.booking.php'); ?>
</section>

<section class="section_titlecontentchild">
    <div class="container">
        <h1>
            <?= $this->lang->line('aboutus'); ?>
        </h1>
        <div class="editable content_location" page-id="<?= $info['page_id'] ?>">
            <?php if (isset($info['content'])) {
                echo $info['content'];
            } ?>
        </div>
    </div>
</section>

<section class="section_photoinfo">
    <div class="owl-carousel owl-theme slide_photoinfo">
        <?php

        foreach ($photos as $index => $imgArray) {
            ?>
            <div class="item">
                <div class="image_images images_contentchild" style=" background:url('<?= $imgArray['image_url']; ?>')"></div>
            </div>
            <?php
        }

        ?>
    </div>

</section>

