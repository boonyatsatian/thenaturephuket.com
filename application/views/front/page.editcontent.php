<?
$title = json_decode($info[0]->title, true);
$content = json_decode($info[0]->content, true);
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="exampleModalLabel">Edit Content</h4>
</div>
<div class="modal-body">
    <form id="modal-form" action="editcontent?save=1" method="post">
        <div class="form-group">
            <label class="control-label">Language:</label>
            <select name="locale" class="form-control">
                <?
                foreach ($languages as $index => $value) {
                    echo "<option value=\"" . $languages[$index]->code . "\">" . $languages[$index]->name . "</option>";
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Title:</label>
            <?
            foreach ($languages as $index => $value) {
                if ($languages[$index]->code != $property[0]->site_locale) {
                    $style = "display:none";
                } else {
                    $style = "";
                }
                ?>
                <input type="text" style="<?= $style ?>" name="title[<? echo $languages[$index]->code; ?>]"
                       class="form-control lang"
                       id="title-<?= $languages[$index]->code ?>"
                       value="<? if (isset($title[$languages[$index]->code])) {
                           echo $title[$languages[$index]->code];
                       } ?>">
                <?
            }
            ?>
        </div>

        <div class="form-group">
            <label for="message-text" class="control-label">Content:</label>
            <?
            foreach ($languages as $index => $value) {
                if ($languages[$index]->code != $property[0]->site_locale) {
                    $style = "display:none!important;";
                } else {
                    $style = "";
                }

                ?>
                <div id="content-<?= $languages[$index]->code ?>" class="lang" style="<?= $style ?>">
        <textarea name="content[<? echo $languages[$index]->code; ?>]" class="form-control ckeditor">
				<? if (isset($content[$languages[$index]->code])) {
                    echo $content[$languages[$index]->code];
                } ?>
</textarea>
                </div>
                <?
            }
            ?>

            <?
            $arr = array('offers', 'accomodations', 'rates');
            if (in_array($info[0]->module, $arr)) {

                if (isset($info[0]->external_url)) {
                    $url = json_decode($info[0]->external_url, true);
                    $url_pc = $url['pc'];
                    $url_mobile = $url['mobile'];
                } else {
                    $url = array();
                    $url_pc = '';
                    $url_mobile = '';
                }


                ?>
                <br/>

                <div class="form-group">
                    <label class="control-label">PC Booking URL</label>
                    <input name="url_pc" class="form-control required" type="text" value="<?= $url_pc ?>"/>
                </div>
                <div class="form-group">
                    <label class="control-label">Mobile Booking URL</label>
                    <input name="url_mobile" class="form-control required" type="text" value="<?= $url_mobile ?>"/>
                </div>
                <?
            }
            ?>

        </div>
        <div class="modal-footer">
            <input name="save" type="submit" value="Save" class="btn btn-primary btn_booknow">
            <input name="page_id" type="hidden" value="<?= $info[0]->page_id ?>"/>
        </div>
    </form>
</div>
<script src="<? echo base_url(); ?>asset/ckeditor/ckeditor.js"></script>
<script src="<? echo base_url(); ?>asset/ckeditor/adapters/jquery.js"></script>
<script>

    $('select[name=locale]').change(function () {
        var locale = this.value;

        $('.lang').hide();
        $('#title-' + this.value).show();
        $('#content-' + this.value).show();
    });

    $('.ckeditor').ckeditor();

    $('#modal-form').submit(function () {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            data: new FormData(this),
            url: "<? echo base_url();?>editcontent?save=1",
        }).done(function (msg) {
            if (msg == 'save') {
                window.location.replace('<?=$_SERVER['HTTP_REFERER']?>');
            } else {
                alert(msg);
            }
        });
        return false;
    });
</script>