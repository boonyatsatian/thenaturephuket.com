<?php include("meta.php");?>
<?php include("header.php");?>



<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center">
                    <h1>Welcome to</h1>
                    <h2 class="space-between-h2-and-p">The Aquamarine Resort & Villa Promotion Special Offer</h2>
                </div>
                <p>Endless Magical Moments at The Aquamarine Resort & Villas
                    <br/><br/>
                    Experience the heavenly pleasures of authentic Thai hospitality high above Kamala Beach with a
                    magnificent vista of the
                    emerald Andaman Sea on the west coast of Phuket, Thailand, where you'll find all the ingredients for
                    most memorable tropical romance.
                    <br/><br/>
                    Romance is always in the air at this ancient Ayudhya-themed sea view retreat with three exotic
                    swimming pools,
                    a spa and private villas nestled among the ancient trees and beautiful ponds of an unspoiled
                    hillside jungle.
                    <br/><br/>
                    Our 186 guestrooms and 16 private hillside villas are a lovely hideaway for couples and families who
                    desire the freedom
                    to relax in privacy and appreciate the beautiful tropical surroundings with amazing sunset views.
                    <br/><br/>
                    A change of scene is never far away with Patong Beach and it's exotic neon-lit nightlife for every
                    persuasion is just a 15-minute drive.
                    <br/><br/>
                    The tranquil and secluded surroundings make Aquamarine Resort & Villas very special honeymoon
                    holiday destination for couples of any persuasion, who desire peace and privacy as well as being
                    close to the wilder side of Phuket's nightlife.
                    <br/><br/>
                    The wireless is now available at the public area only
                    <br/><br/>
                    Our Meeting, Banquet & Conference facilities also make Aquamarine Resort an excellent choice for
                    corporate events.
                    <br/><br/>
                    Dining at Aquamarine Resort is always a treat with continental buffet breakfast and dinner served at
                    the open-air Blue Sea Cafe with a pool and sea view over Kamala Bay. More...
                </p>
                <div class="space-for-arrow"><img src="../../../asset8/images/down.png" alt=""/></div>
            </div>
        </div>
    </div>
</section>

<section class="bg-blue">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 ">
                <div class="text-center"><h1>Promotion</h1>
                    <h2>The Aquamarine Resort & Villas</h2>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4"><a href="promotions.php"><img src="../../../asset8/images/1.png" alt=""/></a></div>
            <div class="col-lg-4 col-md-4 col-sm-4"><a href="promotions.php"><img src="../../../asset8/images/2.png" alt=""/></a></div>
            <div class="col-lg-4 col-md-4 col-sm-4"><a href="promotions.php"><img src="../../../asset8/images/3.png" alt=""/></a></div>
        </div>
    </div>
</section>

<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center">
                    <h1>Facilities We Provide</h1>
                </div>
                <p>Our resort comprises 186 guest rooms and 16 private villas.<br/><br/>
                    Other on-site facilities include three swimming pools, a fitness room, a mini-mart and gift shop,
                    a tailor, tour centre and internet corner. A free daily shuttle services operates to Kamala Beach and Patong Beach.
                </p>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-2 col-sm-2 col-md-4 col-xs-4 social"><img src="../../../asset8/images/social/icon_01.png" alt=""/></div>
            <div class="col-lg-2 col-sm-2 col-md-4 col-xs-4 social"><img src="../../../asset8/images/social/icon_02.png" alt=""/></div>
            <div class="col-lg-2 col-sm-2 col-md-4 col-xs-4 social"><img src="../../../asset8/images/social/icon_03.png" alt=""/></div>
            <div class="col-lg-2 col-sm-2 col-md-4 col-xs-4 social"><img src="../../../asset8/images/social/icon_04.png" alt=""/></div>
            <div class="col-lg-2 col-sm-2 col-md-4 col-xs-4 social"><img src="../../../asset8/images/social/icon_05.png" alt=""/></div>
            <div class="col-lg-2 col-sm-2 col-md-4 col-xs-4 social"><img src="../../../asset8/images/social/icon_06.png" alt=""/></div>
            <div class="clearfix"></div>
            <div class="space-for-arrow"><img src="../../../asset8/images/down-blue.png" alt=""/></div>
        </div>
    </div>
</section>

<section class="bg-blue">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center"><h1>Location</h1>
                    <p>Aquamarine Resort & Villas is in a tranquil hillside location overlooking Kamala beach on the south west coast of Phuket,
                        about one hour's drive from Phuket International Airport, giving our guests beautiful sea views.
                        <br/><br/>
                        Shopping and all sorts of fun in the sun plus the island's most vibrant night life is just a short drive away at the nearby Patong Beach.
                    </p>
                </div>
                <div class="map col-md-12"><img src="../../../asset8/images/map.png" alt=""/></div>
            </div>
        </div>
    </div>
</section>

<?php include("footer.php");?>

