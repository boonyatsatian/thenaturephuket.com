<?php

//$info : Page Detail

?>

<!-- Sojern Tag v6_js, Pixel Version: 1 -->
<script>
    (function () {

        var params = {};

        /* Please do not modify the below code. */
        var cid = [];
        var paramsArr = [];
        var cidParams = [];
        var pl = document.createElement('script');
        var defaultParams = {"vid":"hot","et":"hpr"};
        for(key in defaultParams) { params[key] = defaultParams[key]; };
        for(key in cidParams) { cid.push(params[cidParams[key]]); };
        params.cid = cid.join('|');
        for(key in params) { paramsArr.push(key + '=' + encodeURIComponent(params[key])) };
        pl.type = 'text/javascript';
        pl.async = true;
        pl.src = 'https://beacon.sojern.com/pixel/p/190835?f_v=v6_js&p_v=1&' + paramsArr.join('&');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(pl);
    })();
</script>
<!-- End Sojern Tag -->


<section class="section_slideshow">
    <?php

    //$defaultSlide : Photo Slide

    if (count($slides) > 0) {
        ?>
        <div id="maximage">
            <?php

            foreach ($slides as $index => $slideArray) {
                ?>

                <div class="mc-image ">
                    <?= getImageURL($slideArray['image_url'], 1920, 1100, 'class="editslide" photo-id="' . $slideArray['photo_id'] . '" style="width:100%"') ?>
                    <div class="overlay_slide"></div>
                </div>
                <?php

            }

            ?>

        </div>

        <?php
    }

    ?>
    <div class="section_arrow_slide">
        <a href="" id="arrow_left"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/left_arrow.png"></a>
        <a href="" id="arrow_right"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/right_arrow.png"></a>
    </div>
    <?php include('tpl.booking.php'); ?>
</section>


<section class="section_titleinfo">
    <div class="container">
        <h1>
            <?php
            if (isset($info['title'])) {
                echo $info['title'];
            }
            ?>
        </h1>
        <div class="editable text_info" page-id="<?= $info['page_id'] ?>">
            <p>
                <?php

                if (isset($info['content'])) {
                    echo $info['content'];
                }
                ?>
            </p>
        </div>

        <?php
        if ($info['slug'] == 'deluxe-room') {
            include('roomamenities_deluxe.php');
        }
        if ($info['slug'] == 'deluxe-sea-view') {
            include('roomamenities_deluxe.php');
        }
        if ($info['slug'] == 'deluxe-pool-view') {
            include('roomamenities_deluxe.php');
        }
        if ($info['slug'] == 'deluxe-pool-access') {
            include('roomamenities_deluxe.php');
        }
        if ($info['slug'] == 'deluxe-private-jacuzzi') {
            include('roomamenities_deluxe.php');
        }

        if ($info['slug'] == 'junior-suite') {
            include('roomamenities_suite.php');
        }
        if ($info['slug'] == 'junior-suite-pool-access') {
            include('roomamenities_suite.php');
        }
        if ($info['slug'] == 'grand-suite-two-bedrooms') {
            include('roomamenities_suite.php');
        }
        ?>


        <!--        --><?php
//        if ($info['slug'] == 'deluxe-terrace-room') {
//            include('tpl.roomamenities.php');
//        }
//        if ($info['slug'] == 'deluxe-pool-view') {
//            include('tpl.roomamenities.php');
//        }
//        if ($info['slug'] == 'pool-access-suite') {
//            include('tpl.roomamenities_poolaccesssuite.php');
//        }
//        if ($info['slug'] == 'deluxe-pool-access') {
//            include('tpl.roomamenities.php');
//        }
//        ?>

        <?php

        if (isset($info['external_url']['pc']) && ($info['external_url']['pc'] != "-") && ($info['external_url']['pc'] != "#")) {
            ?>
            <a href="<?= $info['external_url']['pc'] ?>">

                <button class="btn_booknow btn_readmore">
                    <?= $this->lang->line('Book Now'); ?>
                </button>
            </a>
            <?php
        }

        ?>

    </div>
</section>

<section class="section_photoinfo">
        <div class="owl-carousel owl-theme slide_photoinfo">
            <?php

            foreach ($photos as $index => $imgArray) {
                ?>
                <div class="item">
                    <div class="image_images images_contentchild" style=" background:url('<?= $imgArray['image_url']; ?>')"></div>
                </div>
                <?php
            }

            ?>
        </div>

</section>







<?php /*
<div id="container">
  <header>
    <? include("tpl.slide.php");?>
    <? include("tpl.booking.php");?>
  </header>
  <main>
    <div class="centre">
      <div id="left" align="left">
        <div id="content">
        <div class="editable" page-id="<?=$info[0]->page_id?>">
          <h1>
            <?=$title[$locale]?>
          </h1>
          <? echo $content[$locale]?>
         </div>

<?
$sql = "SELECT * \n";
$sql .= "FROM site_page_photo \n";
$sql .= "WHERE page_id = '".$info[0]->page_id."' \n";
$query = $this->db->query($sql);
$photos = $query->result();
if(count($photos)){
?>

<div id="slideshow">
  <div class="info-slider">
    <?
	foreach($photos as $index=>$value){

	?>
    <div class="item"><img alt="" src="<? echo base_url();?>images/<? echo $photos[0]->image_id;?>x600x300.jpg" /></div>
    <?
	}
?></div>
</div>
<script>
$('.info-slider').bxSlider({auto: true, mode: 'fade', controls: false});
</script>

<?
}
?>

<?
$url = json_decode($info[0]->external_url,true);
if(isset($url['pc']) && isset($url['mobile'])){
?>
<a class="button visible-lg" href="<? echo $url['pc']?>">
<span data-hover="<? echo $this->lang->line('Book Now');?>"><? echo $this->lang->line('Book Now');?></span>
</a>

<a class="button visible-xs visible-sm" href="<? echo $url['mobile']?>">
<span data-hover="<? echo $this->lang->line('Book Now');?>"><? echo $this->lang->line('Book Now');?></span>
</a>

<?
}
?>
        </div>
      </div>
      <? include("tpl.sidebar.php");?>
    </div>
  </main>
*/
?>
