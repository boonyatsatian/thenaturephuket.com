<?
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="exampleModalLabel">Edit Photo</h4>
</div>
<div class="modal-body">
  <form id="modal-form" action="editphoto?save=1" enctype="multipart/form-data" method="post">
    <div class="form-group"><img src="<? echo base_url();?>images/<?=$info[0]->image_id?>x800x400" class="img-responsive"> </div>
    <div class="form-group">
    <input name="photo" type="file" />
    </div>
    <div class="modal-footer">
<input name="image_id" type="hidden" value="<? echo $info[0]->image_id;?>" />      
<input type="submit" class="btn btn-primary" value="Save">
      
    </div>
  </form>
</div>
<script>
$('#modal-form').submit(function(){
	$.ajax({
		type: "POST",
		enctype: 'multipart/form-data',
		contentType: false,
		processData: false,	
		data: new FormData(this),		
		url: "editphoto?save=1"
	}).done(function(msg){
		if(msg == "save"){
			window.location.replace('<?=$_SERVER['HTTP_REFERER']?>');
		}else{
			alert("error: "+msg);
		}
	});
	return false;
});
</script>