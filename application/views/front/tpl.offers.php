<h3><? echo $this->lang->line('Check Availability');?></h3>
<form class="form-horizontal">
  <div class="form-group">
    <label class="col-sm-4 control-label"><? echo $this->lang->line('Check In');?></label>
    <div class="col-sm-8">
      <input type="text" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label"><? echo $this->lang->line('Check Out');?></label>
    <div class="col-sm-8">
      <input type="text" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label"><? echo $this->lang->line('Rooms');?></label>
    <div class="col-sm-8">
      <select class="form-control" name="rooms">
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label"><? echo $this->lang->line('Adults');?></label>
    <div class="col-sm-8">
      <select class="form-control" name="adults">
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label"><? echo $this->lang->line('Children');?></label>
    <div class="col-sm-8">
      <select class="form-control" name="child">
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
      <button type="submit" class="btn btn-default"><? echo $this->lang->line('Book Now');?></button>
    </div>
  </div>
</form>

<?
if(isset($offers)){
	echo "<ul class=\"offers\">";
	foreach($offers as $index=>$value){
		$title = json_decode($offers[$index]->title,true);
		$content = json_decode($offers[$index]->content,true);

		$data_arr = explode("<p>",$content[$locale]);
		array_shift($data_arr);
		
		$url = base_url()."offers/".$offers[$index]->slug;
				
		echo "<li>";

		$sql = "SELECT photo_id, image_id FROM site_page_photo WHERE page_id = '".$offers[$index]->page_id."' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		
		if(isset($rs[0]->image_id)){
			echo "<a href=\"".$url."\">";
			echo "<img src=\"images/".$rs[0]->image_id."x400x200\" class=\"img-responsive editphoto\" photo-id=\"".$rs[0]->photo_id."\">";
			echo "</a>";
		}
		echo "<div class=\"editable\" page-id=\"".$offers[$index]->page_id."\">";
		echo "<h4><a href=\"".$url."\">";
		if(isset($title[$locale])){
			echo $title[$locale];
		}else{
			echo $title['en_US'];
		}
		echo "</a></h4>";
		if(isset($data_arr[0])){
			echo "<p>".$data_arr[0]."</p>";		
		}
		echo "</div>";
		echo "</li>";
	}
	echo "</ul>";
?>
<script>
$('.offers').bxSlider({controls: false, auto: true});
</script>
<?
}
?>      