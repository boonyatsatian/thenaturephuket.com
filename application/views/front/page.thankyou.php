



<section class="section_slideshow">
    <?php

    //$defaultSlide : Photo Slide

    if (count($defaultSlide) > 0) {
        ?>
        <div id="maximage">
            <?php

            foreach ($defaultSlide as $index => $slideArray) {
                ?>
                <div class="mc-image ">
                    <?= getImageURL($slideArray['image_url'], 1920, 920, 'class="editslide" photo-id="' . $slideArray['photo_id'] . '" style="width:100%"') ?>
                    <div class="overlay_slide"></div>

                </div>
                <?php

            }

            ?>

        </div>

        <?php
    }

    ?>
    <div class="section_arrow_slide">
        <a href="" id="arrow_left"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/left_arrow.png"></a>
        <a href="" id="arrow_right"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/right_arrow.png"></a>
    </div>
    <?php include('tpl.booking.php'); ?>
</section>

<section class="section_titlecontentchild">
    <div class="container">
        <h3>
            Citygate Residence Resort
        </h3>
        <h1>
            Thank you
        </h1>
        <p>
            Thank you very much for your inquiry with us. we will contact you shortly.
        </p>
        <a href="<?= base_url(); ?>">
            <button class="btn_booknow btn_readmore">
                Home Page
            </button>
        </a>
    </div>
</section>


