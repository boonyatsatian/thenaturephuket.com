<?php

//$info : Page Detail
//$total_page : Total number of pages
//$current_page : Current Page

if(!isset($page_link))
{
	$page_link = $module;
}

if(count($info) > 0)
{
?>

	<?php

	foreach($info as $index=>$infoArray)
	{
		if($index !== 'title')
		{
		?>
            <div class="col-lg-12 accommodation-img" >
                 <div class="col-lg-4 col-md-4">
                 <img src="<?=base_url().'images/'.$infoArray['photo'][0]['image_id']?>x500x500.jpg" class="img-responsive editphoto" photo-id="<?=$infoArray['photo'][0]['photo_id'];?>" width="100%">
                </div>
                <div class="col-lg-8 col-md-8">
                <h2 class="space-between-h2-and-p">
                    <a href="<?=base_url().$page_link.'/'.$infoArray['slug']?>">
                        <?php
						echo $infoArray['title'];
                        ?>
                    </a>
                </h2>
                <p class="editphoto">
					<?php echo $infoArray['content'];?>
                </p>
                </div>
            </div>
		<?php
		}
	}
	?>
    
<?php
}

if($total_page > 1)
{
?>
<section id="rooms" class="list">

<ul class="pagination">
	<?php
	$p = 1;

	while($p <= $total_page)
	{
	?>
  <li class="<?php if($p == $current_page){ echo 'active'; } ?>"><a href="<?=base_url().$page_link.'?page='.$p?>"><?=$p?></a></li>
	<?php
		$p++;
	}

	?>
</ul>
 </section>
<?php
}

?>




