<form name="form_booking" action="https://ibe.hoteliers.guru/ibe/search-room-rate" method="get">
    <div id="ibe" class="reservation_box widget_02">
        <div class="container">
            <div class="reservation">
                <div class="col-xs-4 padding_box">
                    <!--t-datepicker-->
                    <div class="box_inout t-datepicker">
                        <div class="col-xs-6 t-check-in">
                            <p>
                                <?= $this->lang->line('checkin'); ?>
                            </p>

                            <div class="content_ibe">
                                <div class="input_in_out" style="float: left;">
                                    <span class="t-day-check-in"></span>
                                    <span class="t-month-check-in"></span>
                                    <span class="t-year-check-in"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 t-check-out">
                            <p>
                                <?= $this->lang->line('checkout'); ?>
                            </p>

                            <div class="content_ibe">
                                <div class="input_in_out" style="float: left;">
                                    <span class="t-day-check-out"></span>
                                    <span class="t-month-check-out"></span>
                                    <span class="t-year-check-out"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--END t-datepicker-->
                </div>
                <div class="col-xs-3 padding_box">
                    <div class="box_room border_fix">
                        <p>
                            <?= $this->lang->line('pleaseselect'); ?>
                        </p>

                        <div class="content_ibe">
                            <div class="reservation-check">
                                <!--Select Room-->
                                <select class="input-room" id="chknoofroom" name="chknoofroom"></select>
                                <!--END Select Room-->
                                <div class="clearfix"></div>
                                <i class="fa fa-angle-down arrow_index" aria-hidden="true"></i>

                            </div>
                            <!--Box total_adult_child-->
                            <div id="popup_room_list" class="popup_content"></div>
                            <!--End Box total_adult_child-->
                        </div>
                    </div>
                </div>
                <div class="col-xs-3 padding_box">
                    <div class="box_promocode">
                        <p>
                            <?= $this->lang->line('bestrateguarantee'); ?>
                        </p>
                        <div class="content_ibe">
                            <div id="promocode"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 padding_box" id="btn-booknow">
                    <div class="box_book">
                        <div class="content_ibe">
                            <input type="button" id="bookNow" value="<?= $this->lang->line('Book Now'); ?>" class="btnbook_reser" onclick="checkValueSelectRoom()">
                        </div>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</form>


<div class="text-center reservation reservation_boxtitle hidden-xs hidden-sm" style="cursor: pointer; display: none">
    <img src="<?= base_url(); ?>asset8/images/reservation.png" alt=""/>
<!--    <div class="box_reservationtab">-->
<!--        <i class="fa fa-angle-double-down" aria-hidden="true"></i>-->
<!--        <button class="btn_tabreservation">-->
<!--            Online Reservation-->
<!--        </button>-->
<!--    </div>-->

</div>
<!--<div class="text-center reservation hidden-xs hidden-sm" id="close" style="cursor: pointer"><img src="--><?//= base_url(); ?><!--asset8/images/reservation.png" alt=""/></div>-->
<!--<div class="text-center reservation hidden-lg hidden-md"><a href="https://ibe.hoteliers.guru/ibe/En/Demo-Pool-Villa-Hoteliers.Guru-(TH)-Phuket-Town-Phuket-TH"><img src="--><?//= base_url(); ?><!--asset8/images/reservation.png" alt=""/></a></div>-->


<section class="section_reservationmobile">
    <a href="tel:<?= $property['site_phone']; ?>">
        <div class="col-xs-2">
            <i class="fa fa-phone-square" aria-hidden="true"></i>
        </div>
    </a>
    <div class="col-xs-8">
        <a href="<?= $property['site_mobilebooking']; ?>">
            <?= $this->lang->line('Book Now'); ?>
        </a>
    </div>
    <a href="mailto:<?= $property['site_email']; ?>">
        <div class="col-xs-2">
            <i class="fa fa-envelope" aria-hidden="true"></i>
        </div>
    </a>
</section>