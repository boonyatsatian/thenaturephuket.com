<?php

//$offers : Offer Detail


if(count($offers) > 0)
{
?>
<h3><?=$this->lang->line('offers');?></h3>
<div id="accordion" class="margint30">
	<?php

	foreach($offers as $index=>$offerArray)
	{
	?>
  <div class="panel panel-luxen <?php if($index == 0){ echo 'active-panel';}?>">
    <div class="panel-style <?php if($index == 0){ echo 'active';}?> ">
      <h4><span class="plus-box"><i class="fa fa-angle-up"></i></span> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$index?>">
        <?php if(isset($offerArray['title'])){ echo $offerArray['title']; } ?>
        </a></h4>
    </div>
    <div id="collapse<?=$index?>" class="collapse collapse-luxen <?php if($index == 0){ echo 'in'; } ?>">
      <div class="padt20">
      <a href="<?=base_url().'offers/'.$offerArray['slug'];?>">
      <img alt="" src="<?=base_url().'images/'.$offerArray['photo'][0]['image_id']?>x380x250.jpg" class="img-responsive" />
      </a>
      
        <?php if(isset($offerArray['short_content'])){ echo "<p class=\"margint30\">".$offerArray['short_content']."</p>"; } ?>
        <div class="button-style-1 marginb30 margint30">
      	<a href="<?=base_url().'offers/'.$offerArray['slug'];?>"><?=$this->lang->line('Read More');?></a>
      	</div>
      </div>
    </div>
  </div>
	<?php
	}

	?>
</div>
<?php
}

/*
?>
<aside class="layout2">
  <div id="scroll visible-lg"> 
<?
if(count($offers) > 0){
?>  
    <div id="specials" class="list">
      <ul class="slider">

<?
	foreach($offers as $index=>$value){
		$title = json_decode($offers[$index]->title,true);
		$content = json_decode($offers[$index]->content,true);

		if(!isset($title[$locale])){
			$title[$locale] = $title['en_US'];
		}
		
		if(!isset($content[$locale])){
			$content[$locale] = $content['en_US'];
		}
		

		$data_arr = explode("<p>",$content[$locale]);
		array_shift($data_arr);
		
		$url = base_url()."offers/".$offers[$index]->slug;

		$sql = "SELECT photo_id, image_id FROM site_page_photo WHERE page_id = '".$offers[$index]->page_id."' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
?>
        <div class="item"> <img alt="" src="<? echo base_url();?>images/<?=$rs[0]->image_id?>x380x250.jpg" width="380" height="250" />
          <div class="details"> <a href="<?=$url?>">
            <div class="title">
              <?=$title[$locale]?>
            </div>
            <p><? if(isset($data_arr[0])){ echo strip_tags($data_arr[0]);}?></p>
            <div class="button"><span data-hover="<?=$this->lang->line('Read More')?>">
              <?=$this->lang->line('Read More')?>
              </span></div>
            </a> </div>
        </div>
<?
	}
?>
      </ul>
      <div class="nav"></div>
    </div>
  </div>
  
<script>
$('.slider').bxSlider({auto: true, mode: 'fade', pager: false, controls: false});
</script>
 
<?
}
?>  
</aside>
*/?>
