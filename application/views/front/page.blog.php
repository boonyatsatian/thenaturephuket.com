<div class="breadcrumb breadcrumb-1 pos-center">
  <h1><? echo $this->lang->line('blog');?></h1>
</div>
<div class="content"><!-- Content Section -->
  <div class="container margint60">
    <div class="row">
      <div class="col-lg-9">
        <? include("tpl.content.child.php");?>
      </div>
      <div class="col-lg-3"><!-- Sidebar -->
        <? include('tpl.left.php')?>
      </div>
    </div>
  </div>
</div>
