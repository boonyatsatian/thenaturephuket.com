<?php

//$slides : Slide images

if(count($slides)>0)
{
?>

<div class="about-slider margint40"><!-- About Slider -->
  <div class="col-lg-12">
    <div class="flexslider">
      <ul class="slides">
        <?php

		foreach($slides as $index=>$slideArray)
		{
		?>
        <li><img alt="Slider" class="img-responsive" src="<?=base_url().'images/'.$slideArray['image_id']?>x1140x534.jpg" /></li>
        <?php
		}

		?>
      </ul>
    </div>
  </div>
</div>
<?php
}

?>
