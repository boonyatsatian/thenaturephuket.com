<div class="clearfix"></div>
<section class="bg-white marginT150">
    <div class="container">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="text-center ">
                    <h1><?= $this->lang->line('activities'); ?></h1>
                    <h2><?= $this->lang->line('Luxury Hotel'); ?></h2>
                    <p><?= $this->lang->line('contentactivities'); ?></p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-8 col-md-offset-2 "><hr/><div class="clearfix"></div></div>
    </div>

        <?php include("tpl.content.child-for-accommodation.php");?>

</section>