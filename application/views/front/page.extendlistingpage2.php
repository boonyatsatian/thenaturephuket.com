<!-- Sojern Tag v6_js, Pixel Version: 1 -->
<script>
    (function () {

        var params = {};

        /* Please do not modify the below code. */
        var cid = [];
        var paramsArr = [];
        var cidParams = [];
        var pl = document.createElement('script');
        var defaultParams = {"vid":"hot","et":"hpr"};
        for(key in defaultParams) { params[key] = defaultParams[key]; };
        for(key in cidParams) { cid.push(params[cidParams[key]]); };
        params.cid = cid.join('|');
        for(key in params) { paramsArr.push(key + '=' + encodeURIComponent(params[key])) };
        pl.type = 'text/javascript';
        pl.async = true;
        pl.src = 'https://beacon.sojern.com/pixel/p/190835?f_v=v6_js&p_v=1&' + paramsArr.join('&');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(pl);
    })();
</script>
<!-- End Sojern Tag -->


<section class="section_slideshow">
    <div id="maximage">
        <?php

        if (isset($slides) && count($slides) > 0) {
            foreach ($info as $index => $infoArray) {
                if ($index !== 'title') {
                    if (isset($infoArray['photo'][0]['image_id'])) {
                        ?>
                        <div class="mc-image ">
                            <div class="overlay_slide">
                                <img src="<?= $infoArray['photo'][0]['image_url']; ?>"
                                     alt="<?= $property['site_name']; ?>">
                            </div>

                        </div>

                        <?
                    }
                }
            }

        }
        ?>
    </div>
    <div class="section_arrow_slide">
        <a href="" id="arrow_left"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/left_arrow.png"></a>
        <a href="" id="arrow_right"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/right_arrow.png"></a>
    </div>
    <?php include('tpl.booking.php'); ?>
</section>

<section class="section_titlecontentchild">
    <div class="container">
        <h1>
            <?= $this->lang->line('promotions'); ?>
        </h1>
    </div>
</section>

<section class="section_contentchild contentchild_accom">
    <div class="row_contentchild">
        <div class="box_contentchild">
            <?php
            if($module == 'extendlistingpage1'){
                $module = 'experiences';
            }
            if($module == 'extendlistingpage2'){
                $module = 'promotions';
            }
            if($module == 'extendlistingpage3'){
                $module = 'packages';
            }
            if((count($info) > 0)){
                $countData = 0 ;
                foreach ($info as $index => $infoArray) {

                    if ($index !== 'title') {
                        ?>
                        <?php

                        if (isset($infoArray['photo'][0]['image_id'])) {
                            ?>
                            <div class="box_contentchild">

                                <div class="col-xs-6">
                                    <div class="box_images">
                                        <div class="image_images images_contentchild" style=" background:url('<?= $infoArray['photo'][0]['image_url']; ?>')"></div>
                                    </div>

                                    <div class="details_contentchild">
                                        <h2>
                                            <?= $infoArray['title']; ?>
                                        </h2>
                                        <div class="editable" page-id="<?= $infoArray['page_id'] ?>">
                                            <p>
                                                <?php

                                                if (isset($infoArray['short_content'])) {
                                                    echo $infoArray['short_content'];
                                                }

                                                ?>
                                            </p>

                                        </div>
                                        <a href="<?= $infoArray['external_url']['pc'] ?>">
                                            <button class="btn_booknow btn_readmore">
                                                <?= $this->lang->line('Book Now'); ?>
                                            </button>
                                        </a>

                                    </div>


                                </div>



                            </div>

                            <?php
                            $countData++;

                            if ($countData == 2) {
                                $countData = 0;
                                ?>
                                <div class="clearfix"></div>

                                <?php
                            }
                            ?>

                            <?php
                        }

                        ?>
                        <?php
                    }

                }
            }
            ?>
        </div>
    </div>
</section>

