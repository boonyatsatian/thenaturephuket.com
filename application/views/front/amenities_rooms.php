<section class="section_roomamenities">
    <div class="wrap-box">
<!--        <h3>--><?//= $this->lang->line('roomamenities'); ?><!--</h3>-->
        <div class="roomamenities">
            <ul>
                <?php

                $sql = "SELECT * \n";
                $sql .= "FROM site_page \n";
                $sql .= "WHERE module = 'extendlistingpage1' \n";
                $sql .= "ORDER BY display_order DESC , page_id ASC";
                $query = $this->db->query($sql);
                $rs = $query->result();

                if (isset($rs)) {
                    foreach ($rs as $index => $data) {
                        $imgkanda = $rs[$index]->page_id;
                        $img = "SELECT * \n";
                        $img .= "FROM site_page_photo \n";
                        $img .= "WHERE page_id = '" . $imgkanda . "' \n";
                        $img .= "ORDER BY display_order ASC";
                        $img_query = $this->db->query($img);
                        $kn = $img_query->result();
                        $amenities1_id = $data->page_id;
                        $data->menu = json_decode($data->menu, true);
                        $data->title = json_decode($data->title, true);
                        $data->content = json_decode($data->content, true);

                        if (!isset($data->menu[$currentlang['code']]) || $data->menu[$currentlang['code']] == '')
                            $data->menu[$currentlang['code']] = $data->menu['en_US'];

                        if (!isset($data->content[$currentlang['code']]) || $data->content[$currentlang['code']] == '')
                            $data->content[$currentlang['code']] = $data->content['en_US'];

                        if (!isset($data->title[$currentlang['code']]) || $data->title[$currentlang['code']] == '')
                            $data->title[$currentlang['code']] = $data->title['en_US'];

                        $splitContent = explode('</p>', $data->content[$currentlang['code']]);
                        $shortContent = strip_tags($splitContent[0]);

                        foreach ($kn as $indexs => $datas) {
                            ?>
                            <li>
                                <div class="icon-amenities">
                                    <img src="<?= base_url() . 'images/' . $kn[$indexs]->image_id ?>">
                                </div>
                                <?= $data->content[$currentlang['code']] ?>
                            </li>


                            <?
                        }

                    }


                }

                ?>
            </ul>


        </div>
    </div>
</section>