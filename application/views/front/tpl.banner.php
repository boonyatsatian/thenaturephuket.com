<?php

if (isset($banner['image_id'])) {
    ?>

    <style>
        .cnt223 a {
            text-decoration: none;
        }

        .popup {
            width: 100%;
            margin: 0 auto;
            display: none;
            position: fixed;
            z-index: 99999;
            height: 100%;
            background: rgba(0, 0, 0, 0.6);
            top: 0;
        }

        .cnt223 {
            width: 680px;
            /* min-height: 150px; */
            margin: auto;
            background: none;
            position: absolute;
            z-index: 103;
            text-align: center;
            padding: 10px;
            border-radius: 0;
            transition: all 0.5s;
            left: 0;
            right: 0;
            height: auto;
            bottom: 0;
            top: 60px;
        }

        .banner_popup img {
            width: 100%;
        }

        .cnt223 p {
            clear: both;
            color: #555555;
            text-align: center;
        }

        .cnt223 p a {
            color: #d91900;
            font-weight: bold;
        }

        .cnt223 .x {
            float: right;
            position: absolute;
            background: none;
            border-radius: 50px;
            cursor: pointer;
            transition: all 0.5s;
            background: #000;
            width: 50px;
            padding: 10px;
            top: -15px;
            right: -15px;
            border: 2px solid #fff;
            z-index: 2;

        }

        .cnt223 .x:hover {
            transform: rotate(90deg);
        }

        .overf {
            overflow: hidden;
        }

        .slide_popup .owl-dots {
            margin-top: 15px;
            /*display: none;*/
        }

        .slide_popup .owl-dots .active {
            border: 1px solid #fff;
        }

        .slide_popup .owl-dots .owl-dot.active span {
            background: #fff;
        }

        .slide_popup .owl-dots .owl-dot span {
            background: rgba(255, 255, 255, 0.5);
        }

        .slide_popup .owl-dot {
            border: 1px solid rgba(255, 255, 255, 0.5)!important;
        }

        .slide_popup .owl-dot:hover span {
            background: #fff;
        }
        .slide_popup .owl-nav{
            display: block;
        }

        @media screen and (max-width: 1199px) {
            .cnt223 {
                width: 600px;
            }
        }

        @media screen and (max-width: 767px) {
            .cnt223 {
                width: 80%;
            }

            .cnt223 .x {
                width: 40px;
                top: -10px;
                right: -10px;
            }
        }
    </style>
    <?
    $date = date("Y-m-d");
    $sql = "SELECT * FROM site_banner WHERE start <= '" . $date . "' AND end >='" . $date . "'";
    $query = $this->db->query($sql);
    $rs = $query->result();
    $total_photo = count($rs);
    ?>

    <div class='popup'>
        <div class='cnt223'>
            <img src='<?= base_url(); ?>asset_thenaturephuket/images/icon/btn-close.png' alt='quit' class='x' id='x'/>
            <div class="owl-carousel owl-theme slide_popup">
                <? for ($s = 0; $s < $total_photo; $s++) {
                    $sql = "SELECT * FROM site_banner WHERE banner_id = '" . $rs[$s]->banner_id . "'";
                    $query = $this->db->query($sql);
                    $img = $query->result();
                    $image_id = "images/" . $img[0]->image_id;
                    $title = json_decode($img[0]->title, true);
                    $content = json_decode($img[0]->content, true);
                    $url = json_decode($img[0]->url, true); ?>
                    <div class="item">
                        <div class="banner_popup">
                            <a href="<?= $url['url_pc'] ?>" target="_blank">
                                <?= getImageURL($image_id); ?>
                            </a>
                        </div>
                    </div>
                    <?
                } ?>


            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            if ($('.popup').fadeIn()) {
                $('body').addClass('overf')
            } else {
                $('body').removeClass('overf')
            }

            $('.x').click(function () {
                $('.popup').fadeOut();
                $('body').removeClass('overf');
            });
        });

    </script>
    <?php
}

?>
