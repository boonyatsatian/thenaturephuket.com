<section class="section_slideshow">
    <div id="maximage">
        <?php

        if (isset($slides) && count($slides) > 0) {
            foreach ($info as $index => $infoArray) {
                if ($index !== 'title') {
                    if (isset($infoArray['photo'][0]['image_id'])) {
                        ?>
                        <div class="mc-image ">
                            <img src="<?= $infoArray['photo'][0]['image_url']; ?>"
                                 alt="<?= $property['site_name']; ?>">
                            <div class="overlay_slide"></div>

                        </div>
                        <?
                    }
                }
            }

        }
        ?>
    </div>
    <div class="section_arrow_slide">
        <a href="" id="arrow_left"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/left_arrow.png"></a>
        <a href="" id="arrow_right"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/right_arrow.png"></a>
    </div>
    <?php include('tpl.booking.php'); ?>
</section>

<section class="section_titlecontentchild">
    <div class="container">
        <h1>
            <?= $this->lang->line('offers'); ?>
        </h1>
        <h3>
            <?= $property['site_name']; ?>
        </h3>
    </div>
</section>

<section class="section_contentchild">
    <div class="container">
        <div class="row_contentchild">
            <div class="box_contentchild">
                <?php

                foreach ($info as $index => $infoArray) {

                    if ($index !== 'title') {
                        ?>
                        <?php

                        if (isset($infoArray['photo'][0]['image_id'])) {
                            ?>

                            <div class="col-xs-6">
                                <div class="box_images">
                                    <div class="image_images images_contentchild images_contentchildoffers" style=" background:url('<?= $infoArray['photo'][0]['image_url']; ?>')"></div>
                                </div>

                                <div class="details_contentchild">
                                    <h2>
                                        <?= $infoArray['title']; ?>
                                    </h2>
                                    <div class="editable" page-id="<?= $infoArray['page_id'] ?>">
                                        <?php

                                        if (isset($infoArray['content'])) {
                                            echo $infoArray['content'];
                                        }

                                        ?>
                                    </div>
                                    <a href="<?= $infoArray['external_url']['pc'] ?>">
                                        <button class="btn_booknow">
                                            <?= $this->lang->line('Book Now'); ?>
                                        </button>
                                    </a>
                                </div>


                            </div>

                            <?php
                        }

                        ?>
                        <?php
                    }

                }
                ?>
            </div>

        </div>
    </div>
</section>




<!--<section class="bg-white marginT150">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-xs-12 ">-->
<!--                <div class="text-center ">-->
<!--                    <h1>--><?//=$info['title']?><!--</h1>-->
<!--                    <h2>--><?//= $this->lang->line('Luxury Hotel & Promotion Special Offer'); ?><!--</h2>-->
<!--                </div>-->
<!--                <div class="clearfix"></div>-->
<!--            </div>-->
<!--            <div class="col-md-8 col-md-offset-2 "><hr/><div class="clearfix"></div></div>-->
<!--            --><?php //include("tpl.content.child.php");?>
<!--        </div>-->
<!--    </div>-->
<!--</section>-->