<div class="quick-reservation-container">
  <div class="quick-reservation clearfix">
    <div class="title-quick pos-center margint30">
      <h5><? echo $this->lang->line('Check Availability');?></h5>
      <div class="line"></div>
    </div>
    <div class="reserve-form-area">
      <form action="#" method="post" id="ajax-reservation-form">
        <label><? echo $this->lang->line('Check In');?></label>
        <input type="text" id="indate" name="dpd1" class="date-selector" placeholder="&#xf073;" value="<?=date('Y-m-d')?>"/>
        <label><? echo $this->lang->line('Check Out');?></label>
        <input type="text" id="outdate" name="dpd2" class="date-selector" placeholder="&#xf073;" value="<?=date('Y-m-d', time()+86400)?>" />
        <div class="pull-left children clearfix">
          <label><? echo $this->lang->line('Rooms');?></label>
          <select name="rooms" class="pretty-select">
            <option selected="selected" value="1" >1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </div>
        <div class="pull-left type clearfix">
          <label><? echo $this->lang->line('Adults');?></label>
          <select name="adult" class="pretty-select">
            <option selected="selected" value="1" >1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </div>
        <div class="pull-left rooms clearfix">
          <label><? echo $this->lang->line('Children');?></label>
          <select name="children" class="pretty-select">
            <option selected="selected" value="0" >0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </div>
        <div class="pull-left search-button clearfix">
          <div class="button-style-1"> <a id="res-submit" href="#"><? echo $this->lang->line('Book Now');?></a> </div>
        </div>
      </form>
    </div>
  </div>
</div>