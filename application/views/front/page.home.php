<?php

//$info : Page Detail
//$slides : Slide images
//$photos : All Photo Detail
//$contentFooter : Short content of About us
//$blogFooter : Blog List
//$accomodations : Accomodation List

?>

<!-- Sojern Tag v6_js, Pixel Version: 1 -->
<script>
    (function () {

        var params = {};

        /* Please do not modify the below code. */
        var cid = [];
        var paramsArr = [];
        var cidParams = [];
        var pl = document.createElement('script');
        var defaultParams = {"vid":"hot"};
        for(key in defaultParams) { params[key] = defaultParams[key]; };
        for(key in cidParams) { cid.push(params[cidParams[key]]); };
        params.cid = cid.join('|');
        for(key in params) { paramsArr.push(key + '=' + encodeURIComponent(params[key])) };
        pl.type = 'text/javascript';
        pl.async = true;
        pl.src = 'https://beacon.sojern.com/pixel/p/190832?f_v=v6_js&p_v=1&' + paramsArr.join('&');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(pl);
    })();
</script>
<!-- End Sojern Tag -->


<?php include('tpl.banner.php'); ?>

<section class="section_slideshow">

    <?php

    //$defaultSlide : Photo Slide

    if (count($defaultSlide) > 0) {
        ?>
        <div id="maximage">
            <?php

            foreach ($defaultSlide as $index => $slideArray) {
                ?>
                <div class="mc-image ">
                    <?= getImageURL($slideArray['image_url'], 1920, 1000, 'class="editslide" photo-id="' . $slideArray['photo_id'] . '" style="width:100%"') ?>
                    <div class="overlay_slide"></div>
                </div>
                <?php

            }

            ?>

        </div>

        <?php
    }

    ?>
    <div class="section_arrow_slide">
        <a href="" id="arrow_left"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/left_arrow.png"></a>
        <a href="" id="arrow_right"><img src="<?= base_url(); ?>asset_thenaturephuket/images/icon/right_arrow.png"></a>
    </div>


<!--        <div class="index-video-wrapper">-->
<!--            <video autoplay muted loop playsinline src="--><?//= base_url(); ?><!--asset_thenaturephuket/plugins/video/nature2.mp4" id="index-video" type="video/mp4">-->
<!--            </video>-->
<!--        </div>-->

    <?php include('tpl.booking.php'); ?>

</section>


<section class="section_description">
    <div class="container">
        <h3> <?= $info['menu'] ?></h3>
        <h1>
            <?= $info['title'] ?>
        </h1>
        <div class="editable" page-id="<?= $info['page_id'] ?>">
            <?= $info['content'] ?>
        </div>
    </div>
</section>

<?php /****
 * <section class="section_offers">
 * <div class="container">
 * <div class="owl-carousel owl-theme slide_offers">
 * <?php
 *
 * foreach ($offers as $index => $offerArray) {
 * ?>
 * <div class="item">
 * <div class="box_offers">
 * <a href="<?= $offerArray['external_url']['pc'] ?>">
 * <div class="image_images"
 * style=" background:url('<?= ($offerArray['photo'][0]['image_url']); ?>')">
 * <div class="overlay_offers"></div>
 * <div class="details_offers">
 *
 * <h2>
 * <?php if (isset($offerArray['title'])) {
 * echo $offerArray['title'];
 * } ?>
 * </h2>
 * <p>
 * <?php if (isset($offerArray['menu'])) {
 * echo $offerArray['menu'];
 * } ?>
 * </p>
 *
 * </div>
 * </div>
 * </a>
 * <div class="overlay_box"></div>
 * </div>
 * </div>
 * <?php
 * }
 *
 * ?>
 * </div>
 * </div>
 * </section>
 ****/ ?>


<section class="section_accommodation">
    <!--        <h1>-->
    <!--            --><? //= $this->lang->line('accomodations'); ?>
    <!--        </h1>-->
    <!--        <p>-->
    <!--            --><?php
    //            include("tpl.titleaccommodations.php")
    //            ?>
    <!--        </p>-->

    <div class="owl-carousel owl-theme slide_accommodation">
        <?php

        $sql = "SELECT * \n";
        $sql .= "FROM site_page \n";
        $sql .= "WHERE module = 'accomodations' \n";
        $sql .= "ORDER BY display_order DESC , page_id ASC\n";
        $sql .= "LIMIT 15";
        $query = $this->db->query($sql);
        $rs = $query->result();

        $numaccomodations = 1;

        foreach ($rs as $index => $data) {

            $sqlPhoto = "SELECT * \n";
            $sqlPhoto .= "FROM site_page_photo \n";
            $sqlPhoto .= "WHERE page_id = '" . $data->page_id . "' \n";
            $sqlPhoto .= "ORDER BY display_order ASC \n";
            $sqlPhoto .= "LIMIT 1";
            $queryPhoto = $this->db->query($sqlPhoto);
            $rsPhoto = $queryPhoto->result();
            $photo = $rsPhoto[0];
            $imageURL = generateImageURL($photo->image_id);

            $accomodations_id = $data->page_id;
            $data->title = json_decode($data->title, true);
            $data->content = json_decode($data->content, true);

            if ($data->content[$currentlang['code']] == '')
                $data->content[$currentlang['code']] = $data->content['en_US'];

            if ($data->title[$currentlang['code']] == '')
                $data->title[$currentlang['code']] = $data->title['en_US'];

            $splitContent = explode('</p>', $data->content[$currentlang['code']]);
            $shortContent = strip_tags($splitContent[0]);
            ?>
            <div class="item">

                <div class="box_accommodation">
                    <div class="image_images image_accommodation" style=" background:url('<?= $imageURL ?>')"></div>
                    <div class="overlay_box"></div>
                    <a href="<?= base_url() . 'accomodations/' . $data->slug ?>">
                        <div class="details_accommodation">
                            <h1>
                                <?= $data->title[$currentlang['code']] ?>
                            </h1>
                            <h3>
                                <?= $this->lang->line('accomodations'); ?>
                            </h3>

                            <a href="
                            <?= base_url() . 'accomodations/' . $data->slug ?>">
                                <button class="btn_readmore">
                                    <?= $this->lang->line('readmore'); ?>
                                </button>
                            </a>
                        </div>
                    </a>

                </div>
            </div>

            <?php

            $numaccomodations++;
        }
        ?>
    </div>
</section>


<section class="section_facilities">
    <!--        <h1>-->
    <!--            --><? //= $this->lang->line('facilities'); ?>
    <!--        </h1>-->
    <!--        <hr>-->
    <!--        <p>-->
    <!--            --><?php
    //            include("tpl.titlefacilities.php")
    //            ?>
    <!--        </p>-->
    <div class="owl-carousel owl-theme slide_facilities">
        <?php

        $sql = "SELECT * \n";
        $sql .= "FROM site_page \n";
        $sql .= "WHERE module = 'facilities' \n";
        $sql .= "ORDER BY display_order DESC , page_id ASC\n";
        $sql .= "LIMIT 15";
        $query = $this->db->query($sql);
        $rs = $query->result();

        $numFacilities = 1;

        foreach ($rs as $index => $data) {

            $sqlPhoto = "SELECT * \n";
            $sqlPhoto .= "FROM site_page_photo \n";
            $sqlPhoto .= "WHERE page_id = '" . $data->page_id . "' \n";
            $sqlPhoto .= "ORDER BY display_order ASC \n";
            $sqlPhoto .= "LIMIT 1";
            $queryPhoto = $this->db->query($sqlPhoto);
            $rsPhoto = $queryPhoto->result();
            $photo = $rsPhoto[0];
            $imageURL = generateImageURL($photo->image_id);

            $facilities_id = $data->page_id;
            $data->title = json_decode($data->title, true);
            $data->content = json_decode($data->content, true);

            if ($data->content[$currentlang['code']] == '')
                $data->content[$currentlang['code']] = $data->content['en_US'];

            if ($data->title[$currentlang['code']] == '')
                $data->title[$currentlang['code']] = $data->title['en_US'];

            $splitContent = explode('</p>', $data->content[$currentlang['code']]);
            $shortContent = strip_tags($splitContent[0]);
            ?>
            <div class="item">
                <div class="box_accommodation">
                    <div class="image_images image_accommodation" style=" background:url('<?= $imageURL ?>')"></div>
                    <div class="overlay_box"></div>

                    <a href="<?= base_url() . 'facilities/' . $data->slug ?>">
                        <div class="details_facilities">
                            <h1>
                                <?= $data->title[$currentlang['code']] ?>
                            </h1>
                            <h3>
                                <?= $this->lang->line('facilities'); ?>
                            </h3>
                            <a href="<?= base_url() . 'facilities/' . $data->slug ?>">
                                <button class="btn_readmore">
                                    <?= $this->lang->line('readmore'); ?>
                                </button>
                            </a>
                        </div>
                    </a>

                </div>
            </div>


            <?php

            $numFacilities++;
        }
        ?>
    </div>

</section>

