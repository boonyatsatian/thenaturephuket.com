<?
$lang['aboutus'] = "关于我们";
$lang['download'] = "下载";
$lang['location'] = "位置和地图";
$lang['contactus'] = "联系我们";

$lang['home'] = "主页";
$lang['accomodations'] = "住宿";
$lang['facilities'] = "服务与设施";
$lang['rates'] = "价格";
$lang['offers'] = "报价";
$lang['blog'] = "博客";
$lang['event'] = "活动";
$lang['galleries'] = "画廊";
$lang['gallery'] = "画廊";

$lang['Welcome to'] = "欢迎光临";
$lang['The Aquamarine Resort & Villas'] = "The Aquamarine Resort & Villas";
$lang['The Aquamarine Resort & Villa Promotion Special Offer'] = "碧玉别墅度假酒店促销特别优惠";
$lang['contentactivities'] = "活动
通过我们的特殊学习班，让您的一天充满老少咸宜的乐趣，并进一步了解泰国的文化；课程包括：泰国烹饪、水果蔬菜雕刻以及蜡染画。
";

$lang['spa'] = "Spa";
$lang['dining'] = "进餐";
$lang['weddings'] = "婚礼";
$lang['activities'] = "活动";
$lang['Read More'] = "了解更多";
$lang['Submit'] = "ส่ง";

$lang['Check In'] = "登记";
$lang['Check Out'] = "查看";
$lang['Room'] = "房";
$lang['Rooms'] = "房间";
$lang['Adult'] = "成人";
$lang['Adults'] = "成人";
$lang['Child'] = "孩子";
$lang['Children'] = "孩子们";

$lang['Check Availability'] = '检查可用性';
$lang['Book Now'] = '现在预订';

$lang['Edit Content'] = "编辑内容";
$lang['Read More'] = "阅读更多";
$lang['Sign Up'] = "报名";
$lang['News & Offers'] = "新闻和优惠";
$lang['error'] = "Error";
$lang['thankyou'] = "Thankyou";
$lang['nights'] = "Nights";


$lang['Welcome to'] = "Welcome To";
$lang['Luxury Hotel'] = "Luxury Hotel";
$lang['Luxury Hotel & Promotion Special Offer'] = "Luxury Hotel & Promotion Special Offer";
$lang['contentactivities'] = "Fill your day with fun for any age and learn some more about Thai culture with our special classes including Thai Cooking and Fruit & Vegetable Carving.";

$lang['dining'] = "餐饮";
$lang['weddings'] = "婚宴";
$lang['spa'] = "温泉";
$lang['dining'] = "餐饮";
$lang['activities'] = "活动项目";
$lang['location'] = "位置";
$lang['Read More'] = "阅读更多";
$lang['readmore'] = "阅读更多";
$lang['Submit'] = "Submit";
$lang['sitemap'] = "网站地图";
$lang['experiences'] = "经验";
$lang['promotions'] = "优惠";
$lang['packages'] = "套餐";
$lang['amenities'] = "便利设施";
$lang['tel'] = "电话号码";
$lang['checkin'] = "到店日期";
$lang['checkout'] = "离店日期";
$lang['pleaseselect'] = "成人人数/房间";
$lang['bestrateguarantee'] = "优惠码";
$lang['restaurantsandbars'] = "餐厅和酒吧";
$lang['video'] = "視頻";
?>