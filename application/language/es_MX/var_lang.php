<?
$lang['aboutus'] = "About Us";
$lang['download'] = "Download";
$lang['location'] = "Location &amp; Map";
$lang['contactus'] = "Contact Us";

$lang['home'] = "Home";
$lang['accomodations'] = "Accomodations";
$lang['facilities'] = "Facilities";
$lang['rates'] = "Rates";
$lang['offers'] = "Offers";
$lang['blog'] = "Blog";
$lang['event'] = "Events";
$lang['galleries'] = "Galleries";
$lang['gallery'] = "Gallery";

$lang['Check In'] = "Check In";
$lang['Check Out'] = "Check Out";
$lang['Room'] = "Room";
$lang['Rooms'] = "Rooms";
$lang['Adult'] = "Adult";
$lang['Adults'] = "Adults";
$lang['Child'] = "Child";
$lang['Children'] = "Children";

$lang['Check Availability'] = 'Check Availability';
$lang['Book Now'] = 'Book Now';

$lang['Edit Content'] = "Edit Content";
$lang['Read More'] = "Read More";
$lang['Sign Up'] = "Sign Up";
$lang['News & Offers'] = "News & Offers";
$lang['error'] = "Error";
$lang['thankyou'] = "Thankyou";

?>