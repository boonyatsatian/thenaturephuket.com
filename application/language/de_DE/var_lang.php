<?
$lang['aboutus'] = "Über uns";
$lang['download'] = "Herunterladen";
$lang['location'] = "Lage und Karte";
$lang['contactus'] = "Kontaktieren Sie uns";

$lang['home'] = "Zuhause";
$lang['accomodations'] = "Unterkünfte";
$lang['facilities'] = "Einrichtungen";
$lang['rates'] = "Preise";
$lang['offers'] = "Angebote";
$lang['blog'] = "Blog";
$lang['event'] = "Geschehen";
$lang['galleries'] = "Galerien";
$lang['gallery'] = "Galerien";

$lang['Check In'] = "Einchecken";
$lang['Check Out'] = "Überprüfen";
$lang['Room'] = "Zimmer";
$lang['Rooms'] = "Die Zimmer";
$lang['Adult'] = "Erwachsene";
$lang['Adults'] = "Erwachsene";
$lang['Child'] = "Kind";
$lang['Children'] = "Kinder";

$lang['Check Availability'] = 'Verfügbarkeit prüfen';
$lang['Book Now'] = 'buchen Sie jetzt!';

$lang['Edit Content'] = "Inhalt bearbeiten";
$lang['Read More'] = "Lesen Sie weiter";
$lang['Sign Up'] = "Registrieren";
$lang['News & Offers'] = "News und Angebote";
$lang['error'] = "Error";
$lang['thankyou'] = "Thankyou";

?>