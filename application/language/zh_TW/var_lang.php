<?
$lang['aboutus'] = "關於我們";
$lang['download'] = "下載";
$lang['location'] = "位置和地圖";
$lang['contactus'] = "聯繫我們";

$lang['home'] = "家";
$lang['accomodations'] = "住宿地";
$lang['facilities'] = "設施";
$lang['rates'] = "價格";
$lang['offers'] = "報價";
$lang['blog'] = "博客";
$lang['event'] = "活動";
$lang['galleries'] = "畫廊";

$lang['Check In'] = "登記";
$lang['Check Out'] = "查看";
$lang['Room'] = "房";
$lang['Rooms'] = "房間";
$lang['Adult'] = "成人";
$lang['Adults'] = "成人";
$lang['Child'] = "孩子";
$lang['Children'] = "孩子們";

$lang['Check Availability'] = '檢查可用性';
$lang['Book Now'] = '現在預訂';

$lang['Edit Content'] = "編輯內容";
$lang['Read More'] = "閱讀更多";
$lang['Sign Up'] = "報名";
$lang['News & Offers'] = "新聞和優惠";
$lang['error'] = "Error";
$lang['thankyou'] = "Thankyou";

?>