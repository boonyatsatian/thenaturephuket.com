<?
$lang['aboutus'] = "会社概要";
$lang['download'] = "ダウンロード";
$lang['location'] = "所在地と地図";
$lang['contactus'] = "お問い合わせ";

$lang['home'] = "ホーム";
$lang['accomodations'] = "お部屋のご案内";
$lang['facilities'] = "ファシリティ";
$lang['rates'] = "料金";
$lang['offers'] = "オファー";
$lang['blog'] = "ブログ";
$lang['event'] = "イベント";
$lang['galleries'] = "ギャラリー";
$lang['gallery'] = "ギャラリー";

$lang['Check In'] = "チェックイン";
$lang['Check Out'] = "チェックアウト";
$lang['Room'] = "ルーム";
$lang['Rooms'] = "部屋";
$lang['Adult'] = "アダルト";
$lang['Adults'] = "大人";
$lang['Child'] = "チャイルド";
$lang['Children'] = "子供";

$lang['Check Availability'] = '空室状況をチェックします';
$lang['Book Now'] = '今すぐ予約';

$lang['Edit Content'] = "編集内容";
$lang['Read More'] = "続きを読みます";
$lang['Sign Up'] = "サインアップ";
$lang['News & Offers'] = "ニュース＆オファー";
$lang['error'] = "Error";
$lang['thankyou'] = "Thankyou";
?>