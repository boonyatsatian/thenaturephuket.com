<?
$lang['aboutus'] = "Riguardo A Noi";
$lang['download'] = "Scarica";
$lang['location'] = "Ubicazione e mappa";
$lang['contactus'] = "Contattaci";

$lang['home'] = "casa";
$lang['accomodations'] = "Alloggi";
$lang['facilities'] = "Servizi";
$lang['rates'] = "Prezzi";
$lang['offers'] = "Offerte";
$lang['blog'] = "Blog";
$lang['event'] = "Eventi";
$lang['galleries'] = "Gallerie";
$lang['gallery'] = "Gallerie";

$lang['Check In'] = "data di arrivo";
$lang['Check Out'] = "data di partenza";
$lang['Room'] = "stanza";
$lang['Rooms'] = "Camere";
$lang['Adult'] = "adulto";
$lang['Adults'] = "adulti";
$lang['Child'] = "bambino";
$lang['Children'] = "bambini";

$lang['Check Availability'] = 'Verificare La Disponibilità';
$lang['Book Now'] = 'Prenota Ora';

$lang['Edit Content'] = "Modifica contenuto";
$lang['Read More'] = "Per Saperne Di Più";
$lang['Sign Up'] = "Iscriviti";
$lang['News & Offers'] = "Notizie & Offerte";
$lang['error'] = "Error";
$lang['thankyou'] = "Thankyou";

?>