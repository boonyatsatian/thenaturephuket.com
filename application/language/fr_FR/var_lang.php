<?
$lang['aboutus'] = "A Propos De Nous";
$lang['download'] = "Télécharger";
$lang['location'] = "Situation et carte";
$lang['contactus'] = "Contactez Nous";

$lang['home'] = "maison";
$lang['accomodations'] = "hébergement";
$lang['facilities'] = "aménagements";
$lang['rates'] = "Tarifs";
$lang['offers'] = "Offres";
$lang['blog'] = "Blog";
$lang['event'] = "événements";
$lang['galleries'] = "Galeries";
$lang['gallery'] = "Galeries";

$lang['Check In'] = "date d'arrivée";
$lang['Check Out'] = "date de départ";
$lang['Room'] = "salle";
$lang['Rooms'] = "chambres";
$lang['Adult'] = "adulte";
$lang['Adults'] = "adultes";
$lang['Child'] = "enfant";
$lang['Children'] = "enfants";

$lang['Check Availability'] = 'Vérifier la disponibilité';
$lang['Book Now'] = 'Réserver';

$lang['Edit Content'] = "Modifier le contenu";
$lang['Read More'] = "En Savoir Plus";
$lang['Sign Up'] = "signer";
$lang['News & Offers'] = "Nouvelles et Offres";
$lang['error'] = "Error";
$lang['thankyou'] = "Thankyou";

?>