<?
$lang['aboutus'] = "회사 소개";
$lang['download'] = "다운로드";
$lang['location'] = "위치 및지도";
$lang['contactus'] = "문의";

$lang['home'] = "홈";
$lang['accomodations'] = "방을";
$lang['facilities'] = "시설";
$lang['rates'] = "요금";
$lang['offers'] = "상품";
$lang['blog'] = "블로그";
$lang['event'] = "이벤트";
$lang['galleries'] = "갤러리";
$lang['gallery'] = "갤러리";

$lang['Welcome to'] = "환영합니다";
$lang['The Aquamarine Resort & Villas'] = "아쿠아마린 리조트 & 빌라";
$lang['The Aquamarine Resort & Villa Promotion Special Offer'] = "아쿠아마린 리조트 & 빌라 특별 할인";
$lang['contentactivities'] = "남녀노소 재미있게 태국의 문화를 배울 수 있는 특별 교실을 만나보세요: 태국 요리교실, 과일 & 야채 조각교실, 바틱 그림교실.";

$lang['spa'] = "스파";
$lang['dining'] = "식사";
$lang['weddings'] = "결혼식";
$lang['activities'] = "활동";
$lang['Read More'] = "자세히보기";
$lang['Submit'] = "제출하다";

$lang['Check In'] = "체크인";
$lang['Check Out'] = "체크 아웃";
$lang['Room'] = "방";
$lang['Rooms'] = "객실";
$lang['Adult'] = "성인";
$lang['Adults'] = "성인";
$lang['Child'] = "아이";
$lang['Children'] = "어린이";

$lang['Check Availability'] = '예약 가능 여부 확인';
$lang['Book Now'] = '예약';

$lang['Edit Content'] = "편집 내용";
$lang['Read More'] = "자세히보기";
$lang['Sign Up'] = "회원 가입";
$lang['News & Offers'] = "뉴스 및 특별 행사";
$lang['error'] = "Error";
$lang['thankyou'] = "Thankyou";

?>