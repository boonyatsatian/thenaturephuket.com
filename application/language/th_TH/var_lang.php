<?
$lang['aboutus'] = "เกี่ยวกับเรา";
$lang['download'] = "ดาวน์โหลด";
$lang['location'] = "ที่อยู่และแผนที่";
$lang['contactus'] = "ติดต่อเรา";

$lang['home'] = "หน้าแรก";
$lang['accomodations'] = "ห้องพัก";
$lang['facilities'] = "สิ่งอำนวยความสะดวก";
$lang['rates'] = "ราคา";
$lang['offers'] = "ข้อเสนอพิเศษ";
$lang['blog'] = "บล็อก";
$lang['event'] = "เหตุการณ์";
$lang['galleries'] = "แกลเลอรี่";
$lang['gallery'] = "แกลเลอรี่";

$lang['Welcome to'] = "ยินดีต้อนรับเข้าสู่";
$lang['The Aquamarine Resort & Villas'] = "อความารีน รีสอร์ท แอนด์ วิลล่า";
$lang['The Aquamarine Resort & Villa Promotion Special Offer'] = "ข้อเสนอโปรโมชั่นพิเศษจากอความารีน รีสอร์ท แอนด์ วิลล่า";
$lang['contentactivities'] = "เติมเต็มวันของคุณด้วยความสนุกสำหรับทุกวัย และเรียนรู้วัฒนธรรมไทยจากชั้นเรียนพิเศษของเราที่มีทั้งการสอนการทำอาหารไทย และการแกะสลักผักและผลไม้";

$lang['spa'] = "สปา";
$lang['dining'] = "ห้องอาหาร";
$lang['weddings'] = "งานแต่งงาน";
$lang['activities'] = "กิจกรรม";
$lang['Read More'] = "อ่านทั้งหมด";
$lang['Submit'] = "ส่ง";

$lang['Check In'] = "เช็คอิน";
$lang['Check Out'] = "เช็คเอาท์";
$lang['Room'] = "ห้อง";
$lang['Rooms'] = "ห้อง";
$lang['Adult'] = "ผู้ใหญ่";
$lang['Adults'] = "ผู้ใหญ่";
$lang['Child'] = "เด็ก";
$lang['Children'] = "เด็ก";

$lang['Check Availability'] = 'ตรวจสอบสถานะ';
$lang['Book Now'] = 'จองตอนนี้';

$lang['Edit Content'] = "แก้ไขเนื้อหา";
$lang['Read More'] = "อ่านต่อ";
$lang['Sign Up'] = "ลงทะเบียน";
$lang['News & Offers'] = "ข่าวและโปรโมชั่น";
$lang['error'] = "Error";
$lang['thankyou'] = "Thankyou";
$lang['sitemap'] = "แผนผังเว็บไซต์";
$lang['roomamenities'] = "สิ่งอำนวยความสะดวกในห้อง";
$lang['activities'] = "กิจกรรม";
$lang['greensarispa'] = "สปา";

$lang['dining'] = "อาหารและเครื่องดื่ม";
$lang['weddings'] = "งานแต่งงาน";
$lang['spa'] = "สปา";
$lang['dining'] = "อาหารและเครื่องดื่ม";
$lang['activities'] = "กิจกรรม";
$lang['location'] = "ที่ตั้ง";
$lang['Read More'] = "อ่านต่อ";
$lang['readmore'] = "อ่านต่อ";
$lang['Submit'] = "ยืนยัน";
$lang['sitemap'] = "แผนผังเว็บไซต์";
$lang['experiences'] = "ประสบการณ์";
$lang['promotions'] = "โปรโมชั่น";
$lang['packages'] = "แพคเกจ";
$lang['amenities'] = "สิ่งอำนวยความสะดวก";
$lang['tel'] = "โทร";
$lang['checkin'] = "เช็คอิน";
$lang['checkout'] = "เช็คเอาท์";
$lang['pleaseselect'] = "โปรดเลือก";
$lang['bestrateguarantee'] = "รับประกันราคาดีที่สุด";
$lang['restaurantsandbars'] = "ร้านอาหารและบาร์";
$lang['video'] = "วีดีโอ";
?>