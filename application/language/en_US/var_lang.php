<?
$lang['aboutus'] = "About Us";
$lang['download'] = "Download";
$lang['contactus'] = "Contact Us";

$lang['home'] = "Home";
$lang['accomodations'] = "Accommodations";
$lang['facilities'] = "Facilities";
$lang['rates'] = "Rates";
$lang['offers'] = "Special Offers";
$lang['blog'] = "Blog";
$lang['event'] = "Events";
$lang['galleries'] = "Galleries";
$lang['gallery'] = "Gallery";
$lang['Reservation'] = "Reservation";

$lang['Check In'] = "Check In";
$lang['Check Out'] = "Check Out";
$lang['Room'] = "Room";
$lang['Rooms'] = "Rooms";
$lang['Adult'] = "Adult";
$lang['Adults'] = "Adults";
$lang['Child'] = "Child";
$lang['Children'] = "Children";

$lang['Check Availability'] = 'Check Availability';
$lang['Book Now'] = 'Book Now';

$lang['Edit Content'] = "Edit Content";
$lang['Read More'] = "Read More";
$lang['Sign Up'] = "Sign Up";
$lang['News & Offers'] = "News & Offers";
$lang['error'] = "Error";
$lang['thankyou'] = "Thankyou";
$lang['nights'] = "Nights";


$lang['Welcome to'] = "Welcome To";
$lang['Luxury Hotel'] = "Luxury Hotel";
$lang['Luxury Hotel & Promotion Special Offer'] = "Luxury Hotel & Promotion Special Offer";
$lang['contentactivities'] = "Fill your day with fun for any age and learn some more about Thai culture with our special classes including Thai Cooking and Fruit & Vegetable Carving.";

$lang['dining'] = "Dining";
$lang['weddings'] = "Weddings";
$lang['spa'] = "Spa";
$lang['dining'] = "Dining";
$lang['activities'] = "Activities";
$lang['location'] = "Location";
$lang['Read More'] = "Read More";
$lang['readmore'] = "Read More";
$lang['Submit'] = "Submit";
$lang['sitemap'] = "Site Map";
$lang['experiences'] = "Experiences";
$lang['promotions'] = "Promotions";
$lang['packages'] = "Packages";
$lang['amenities'] = "Amenities";
$lang['tel'] = "Tel";
$lang['checkin'] = "Check In";
$lang['checkout'] = "Check Out";
$lang['pleaseselect'] = "Please Select";
$lang['bestrateguarantee'] = "Best Rate Guarantee";
$lang['restaurantsandbars'] = "Restaurants and Bars";
$lang['video'] = "Video";
?>

