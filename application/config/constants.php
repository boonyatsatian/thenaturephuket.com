<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Title for Extend Page
|--------------------------------------------------------------------------
 */
define('TITLE_EXTEND_PAGE1', 'Site Map');
define('TITLE_EXTEND_PAGE2', 'Title Experiences');
define('TITLE_EXTEND_PAGE3', 'Title Restaurants and Bars');

/*
|--------------------------------------------------------------------------
| Title for Extend Listing Page
|--------------------------------------------------------------------------
 */
$titleExtendPage = array(
	'extendlistingpage1' => 'Experiences',
	'extendlistingpage2' => 'Promotions',
	'extendlistingpage3' => 'Packages',
	'extendlistingpage4' => 'Room Amenities Deluxe',
	'extendlistingpage5' => 'Room Amenities Suite',
	'extendlistingpage6' => 'Restaurants and Bars',
);

/*
|--------------------------------------------------------------------------
| URL for Extend Listing Page
|--------------------------------------------------------------------------
 */
$extendPageLink = array(
	'extendlistingpage1' => 'experiences',
	'extendlistingpage2' => 'promotions',
	'extendlistingpage3' => 'packages',
	'extendlistingpage4' => 'roomamenities_deluxe',
	'extendlistingpage5' => 'roomamenities_suite',
	'extendlistingpage6' => 'restaurantsandbars',
);

define('TITLE_EXTEND_LISTING_PAGE', serialize($titleExtendPage));
define('LINK_EXTEND_LISTING_PAGE', serialize($extendPageLink));

/*
|--------------------------------------------------------------------------
| Menu List
|--------------------------------------------------------------------------
 */
$pageMenuList = array(
	'homepage' => 'Home Page',
//	'aboutus' => 'About Us',
	'location' => 'Location',
	'contactus' => 'Contact Us',
	'site-map' => 'Site Map',
);

$listingPageMenuList = array(
	'accomodations' => 'Accommodations',
	'roomamenities_deluxe' => '- Room Amenities Deluxe',
	'roomamenities_suite' => '- Room Amenities Suite',
	'experiences' => 'Experiences',
	'title_experiences' => '- Title Experiences',
	'restaurantsandbars' => 'Restaurants and Bars',
    'title_restaurantsandbars' => '- Title Restaurants and Bars',
//    'offers' => 'Special Offers',
//	'activities' => 'Activities',
//	'titleactivities' => '- Title Activities',
//	'weddings' => 'Weddings',
	'facilities' => 'Facilities',
	'promotions' => 'Promotions',
	'packages' => 'Packages',
//	'greensarispa' => 'Green Sari Spa',
//	'titlegreensarispa' => '- Title Green Sari Spa',
//	'spa' => 'Spa',
//	'activities' => 'Activities',
	'gallery' => 'Gallery',
//	'location' => 'Location',
//	'contactus' => 'Contactus',
//	'thankyou' => 'Thankyou',
//	'error' => 'Error'
);

$settingMenuList = array(
	'property' => 'Property',
	'banner' => 'Banner',
	'social' => 'Social Media',
	'languages' => 'Languages',
	'color' => 'Color',
	'users' => 'Users',
	'seo' =>'SEO'
);

define('PAGE_MENU_LIST', serialize($pageMenuList));
define('LISTING_PAGE_MENU_LIST', serialize($listingPageMenuList));
define('SETIING_MENU_LIST', serialize($settingMenuList));


/*
|--------------------------------------------------------------------------
| Module Name
|--------------------------------------------------------------------------
 */
define('MODULE_TYPE_HOMEPAGE', 'homepage');
define('MODULE_TYPE_ABOUTUS', 'aboutus');
define('MODULE_TYPE_ACCOMODATION', 'accomodations');
define('MODULE_TYPE_BLOG', 'blog');
define('MODULE_TYPE_CONTACTUS', 'contactus');
define('MODULE_TYPE_DOWNLOAD', 'download');
define('MODULE_TYPE_FACILITIES', 'facilities');
define('MODULE_TYPE_EVENT', 'event');
define('MODULE_TYPE_GALLERY', 'gallery');
define('MODULE_TYPE_LOCATION', 'location');
define('MODULE_TYPE_OFFERS', 'offers');
define('MODULE_TYPE_RATES', 'rates');
define('MODULE_TYPE_THANKYOU', 'thankyou');
define('MODULE_TYPE_ERROR', 'error');
define('MODULE_TYPE_EXTENDPAGE', 'extendpage');
define('MODULE_TYPE_EXTENDLISTINGPAGE', 'extendlistingpage');


/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
