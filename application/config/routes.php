<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// frontend
$route['default_controller'] = 'Front_home/index';
$route['error404'] = "Front_home/error404";

$route['homepage'] = "Front_home/index";
$route['aboutus'] = "Front_aboutus/index";
$route['download'] = "Front_download/index";
$route['location'] = "Front_location/index";
$route['contactus'] = "Front_contactus/index";
$route['file'] = "Front_file/index";

$route['accomodations'] = "Front_accomodations/index";
$route['accomodations/(:any)'] = "Front_accomodations/info/$1";

$route['facilities'] = "Front_facilities/index";
$route['facilities/(:any)'] = "Front_facilities/info/$1";
/*
$route['rates'] = "Front_rates/index";
$route['rates/(:any)'] = "Front_rates/info/$1";*/

$route['offers'] = "Front_offers/index";
$route['offers/(:any)'] = "Front_offers/info/$1";
/*
$route['blog'] = "Front_blog/index";
$route['blog/(:any)'] = "Front_blog/info/$1";*/

/*$route['event'] = "Front_event/index";
$route['event/(:any)'] = "Front_event/info/$1";*/

$route['gallery'] = "Front_galleries/index";

$route['editcontent'] = "Front_editcontent/index";
$route['editphoto'] = "Front_editphoto/index";
$route['editslide'] = "Front_editslide/index";

$route['locale'] = 'Front_locale/index';

$route['images/(:any)'] = 'Front_images/photo/$1';

$route['thankyou'] = 'Front_thankyou/index';
$route['thankyou/(:any)'] = "Front_thankyou/info/$1";

$route['error'] = 'Front_error/index';
$route['error/(:any)'] = "Front_error/info/$1";

//---------------------------------------------------------//
//------------------- Front Extend Page -------------------//

$route['site-map'] = "Front_extendpage/index/1";
$route['title_experiences'] = "Front_extendpage/index/2";
$route['title_restaurantsandbars'] = "Front_extendpage/index/3";

//---------------------------------------------------------//
//--------------- Front Extend Listing Page ---------------//
$route['experiences'] = "Front_extendlistingpage/index/1";
$route['experiences/(:any)'] = "Front_extendlistingpage/info/$1/1";
$route['promotions'] = "Front_extendlistingpage/index/2";
$route['promotions/(:any)'] = "Front_extendlistingpage/info/$1/2";
$route['packages'] = "Front_extendlistingpage/index/3";
$route['packages/(:any)'] = "Front_extendlistingpage/info/$1/3";
$route['roomamenities_deluxe'] = "Front_extendlistingpage/index/4";
$route['roomamenities_deluxe/(:any)'] = "Front_extendlistingpage/info/$1/4";
$route['roomamenities_suite'] = "Front_extendlistingpage/index/5";
$route['roomamenities_suite/(:any)'] = "Front_extendlistingpage/info/$1/5";
$route['restaurantsandbars'] = "Front_extendlistingpage/index/6";
$route['restaurantsandbars/(:any)'] = "Front_extendlistingpage/info/$1/6";




// --------------------------------- end Frontend -------------------------------


// backend
$route['admin'] = 'admin_home/index';
$route['admin/home'] = 'admin_home/index';

$route['admin/homepage'] = 'admin_homepage/index';
$route['admin/aboutus'] = 'admin_aboutus/index';
$route['admin/download'] = 'admin_download/index';
$route['admin/location'] = 'admin_location/index';
$route['admin/contactus'] = 'admin_contactus/index';

$route['admin/accomodations'] = 'admin_accomodations/index';
$route['admin/facilities'] = 'admin_facilities/index';
/*$route['admin/rates'] = 'admin_rates/index';*/
$route['admin/offers'] = 'admin_offers/index';
/*$route['admin/blog'] = 'admin_blog/index';*/
/*$route['admin/event'] = 'admin_event/index';*/
$route['admin/gallery'] = 'admin_gallery/index';
$route['admin/thankyou'] = 'admin_thankyou/index';
$route['admin/error'] = 'admin_error/index';

$route['admin/newsletter'] = 'admin_newsletter/index';
$route['admin/booking'] = 'admin_booking/index';

$route['admin/image'] = 'admin_image/index';
$route['admin/seo'] = 'admin_seo/index';


$route['admin/property'] = 'admin_property/index';
$route['admin/social'] = 'admin_social/index';
$route['admin/languages'] = 'admin_languages/index';
$route['admin/users'] = 'admin_users/index';
$route['admin/banner'] = 'admin_banner/index';
$route['admin/color'] = 'admin_color/index';

$route['admin/login'] = 'admin_login/index';
$route['admin/forgetpassword'] = 'admin_forgetpassword/index';
$route['admin/logout'] = 'admin_logout/index';

//---------------------------------------------------------//
//------------------- Admin Extend Page -------------------//

$route['admin/site-map'] = 'admin_extendpage/index/1';
$route['admin/title_experiences'] = 'admin_extendpage/index/2';
$route['admin/title_restaurantsandbars'] = 'admin_extendpage/index/3';

//---------------------------------------------------------//
//--------------- Admin Extend Listing Page ---------------//

$route['admin/experiences'] = "admin_extendlistingpage/index/1";
$route['admin/promotions'] = "admin_extendlistingpage/index/2";
$route['admin/packages'] = "admin_extendlistingpage/index/3";
$route['admin/roomamenities_deluxe'] = "admin_extendlistingpage/index/4";
$route['admin/roomamenities_suite'] = "admin_extendlistingpage/index/5";
$route['admin/restaurantsandbars'] = "admin_extendlistingpage/index/6";



//---------------------------------------------------------//
//----------------- Implement by hoteliers ----------------//
$route['admin/event_calendar'] = 'AdminEventCalendar/index';
$route['admin/event_calendar/add'] = 'AdminEventCalendar/add';
$route['admin/event_calendar/edit/(:any)'] = 'AdminEventCalendar/edit/$1';
$route['admin/event_calendar/delete/(:any)'] = 'AdminEventCalendar/delete/$1';
$route['admin/event_calendar/detail/(:any)'] = 'AdminEventCalendar/getEventInfo/$1';
$route['admin/event_calendar/(:any)/(:any)'] = 'AdminEventCalendar/index/$1/$2';

// --------------------------------- end backend -------------------------------




$route['404_override'] = 'Front_home/error404';
$route['translate_uri_dashes'] = FALSE;
