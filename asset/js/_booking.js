$().ready(function() {

    $("#indate").datepicker({minDate: 0, onSelect: customStart, dateFormat: "yy-mm-dd", firstDay: 1, changeFirstDay: false, altField: "#indate",
altFormat: "yy-mm-dd", autoclose: true});
    $("#outdate").datepicker({ beforeShow: customEnd, dateFormat: "yy-mm-dd", firstDay: 1, changeFirstDay: false, altField: "#outdate",
altFormat: "yy-mm-dd", autoclose: true});
    var currentTime = new Date();
    var smonth = currentTime.getMonth() + 1;
    var smonth = (smonth < 10) ? '0' + smonth : smonth;
    var sday = currentTime.getDate();
    var sday = (sday < 10) ? '0' + sday : sday;
    var syear = currentTime.getFullYear();
    $("#indate").val( sday+ "-" + smonth + "-" +syear);
    $("#indate").val( syear+ "-" + smonth + "-" +sday);

    var outdate = new Date(currentTime.setDate(currentTime.getDate() + 1));
    var emonth = outdate.getMonth() + 1;
    var emonth = (emonth < 10) ? '0' + emonth : emonth;
    var eday = currentTime.getDate();
    var eday = (eday < 10) ? '0' + eday : eday;
    var eyear = currentTime.getFullYear();
    $("#outdate").val(eday + "-" + emonth + "-" +eyear);
    $("#outdate").val( eyear+ "-" + emonth + "-" +eday);
                  
 });

function parseDate(input) {
    var parts = input.match(/(\d+)/g);
    // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
    return new Date( parts[2], parts[1]-1,parts[0]); // months are 0-based
}

function customStart(input) {
    var newDate = $(this).datepicker("getDate");
    if (newDate) {
        newDate.setDate(newDate.getDate() + 1);
    }
    $("#outdate").datepicker().datepicker("enable");
    $("#outdate").datepicker("setDate", newDate);
    dateDifference();
}


function customEnd(input) {
    var newDate = $("#indate").datepicker("getDate");
    if (newDate) {
        newDate.setDate(newDate.getDate() + 1);
    }
    $("#outdate").datepicker("option", "minDate", newDate);
}


function available(frmObj){
    var form = document.getElementById(frmObj);
    form.setAttribute("method", "get");
    form.setAttribute("action", "https://secure.widediscovery.com/v2.1/wdsystem.php");

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "hotel");
    HotelhiddenField.setAttribute("name", "hotel");
    HotelhiddenField.setAttribute("value", "00005");
    form.appendChild(HotelhiddenField);

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "sd");
    HotelhiddenField.setAttribute("name", "sd");
    HotelhiddenField.setAttribute("value", $('#indate').val());
    form.appendChild(HotelhiddenField);

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "ed");
    HotelhiddenField.setAttribute("name", "ed");
    HotelhiddenField.setAttribute("value", $('#outdate').val());
    form.appendChild(HotelhiddenField);

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "rooms");
    HotelhiddenField.setAttribute("name", "rooms");
    HotelhiddenField.setAttribute("value",  $('#chknoofroom :selected').val());
    form.appendChild(HotelhiddenField);

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "adult");
    HotelhiddenField.setAttribute("name", "adult");
    HotelhiddenField.setAttribute("value", $('#chknoadult :selected').val());
    form.appendChild(HotelhiddenField);

    var HotelhiddenField = document.createElement("input");
    HotelhiddenField.setAttribute("type", "hidden");
    HotelhiddenField.setAttribute("id", "child");
    HotelhiddenField.setAttribute("name", "child");
    HotelhiddenField.setAttribute("value",  $('#chknochild :selected').val());
    form.appendChild(HotelhiddenField);

    form.submit();
}

function dateDifference(){
    var strDate1=document.getElementById("indate").value;
    var strDate2=document.getElementById("outdate").value;
    datDate1= parseDate(strDate1);
    datDate2= parseDate(strDate2);
    //if(((datDate2-datDate1)/(24*60*60*1000))>=0){
    //    document.getElementById("night").value=((datDate2-datDate1)/(24*60*60*1000));
    //}
}