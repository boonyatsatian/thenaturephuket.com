<?php
	
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	echo 'Start...<br>';

	symlink('../_corecms/data.sql', 'data.sql');
	symlink('../_corecms/index.php', 'index.php');
	symlink('../_corecms/system', 'system');
	
	symlink('../../_corecms/application/index.html', 'application/index.html');
	symlink('../../_corecms/application/controllers', 'application/controllers');
	symlink('../../_corecms/application/core', 'application/core');
	symlink('../../_corecms/application/helpers', 'application/helpers');
	symlink('../../_corecms/application/hooks', 'application/hooks');
	symlink('../../_corecms/application/libraries', 'application/libraries');
	symlink('../../_corecms/application/models', 'application/models');
	symlink('../../_corecms/application/third_party', 'application/third_party');
	symlink('../../../_corecms/application/views/admin', 'application/views/admin');

	chmod('uploads', 0777);
	chmod('application/cache', 0777);
	chmod('application/logs', 0777);
	chmod('application/views/front/imagecache', 0777);

	echo 'Set up success.';